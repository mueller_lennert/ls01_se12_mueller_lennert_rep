package abArray1;

public class Lotto {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Dieses Programm gibt schleifenbasiert Lottozahlen aus und �berpr�ft\ndann, ob die Zahlen 12 und 13 in der Ziehung enthalten waren\n");
		
		byte[] array = {3,7,12,18,37,42};
		boolean lotto12 = false, lotto13 = false;

		String zahlenListe = "[ ";
		for (byte i = 1; i < array.length; i++) {
		   zahlenListe += array[i] + " "; 
		}
		
		zahlenListe += "]";
		System.out.printf("%s", zahlenListe);
	    
		System.out.print("\n\n");
		for (byte i = 1; i < array.length; i++) {
			if (array[i] == 12) {
				lotto12 = true;
				System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
			}
			if (array[i] == 13) {
				lotto13 = true;
				System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
			}
		}
		
		if (lotto12 == false) {
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
		}
		if (lotto13 == false) {
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		}
		
	}
}
