package abArray1;

import java.util.Scanner;

public class Palindrom {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Dieses Programm gibt Daten in umgekehrter Reihenfolge aus\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Legen Sie die L�nge vom Array fest: ");
		int laenge = sc.nextInt();
		char[] tastatur = new char[laenge];
		
		System.out.println("Soll das umgekehrte Array roh ausgegeben werden?");
		System.out.print("J f�r ja, alles andere = nein: ");
		char ausgabeEinstellung = sc.next().charAt(0);
		
		System.out.print("Geben Sie bis zu " + laenge + " Zeichen ein und dr�cken Sie anschlie�end ENTER: ");
		tastatur = sc.next().toCharArray();

		System.out.print("\n");
		if (ausgabeEinstellung != 'J' && ausgabeEinstellung != 'j') {
			for (int i = tastatur.length-1; i >= 0; i--) {
				System.out.println("Wert bei Array Index " + i + " ist: " + tastatur[i]);
				if (tastatur[i] == ';') {
					System.out.print("\n");
				}	
			}
		}
		
		else {
			for (int i = tastatur.length-1; i >= 0; i--) {
				System.out.print(tastatur[i]);
				if (tastatur[i] == ';') {
					System.out.print("\n");
				}	
			}
		}		
		
		sc.close();
	}
}
