package abArray1;

public class UngradeZahlen {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Dieses Programm benutzt Schleifen um ein Array\nmit ungeraden Zahlen zu f�llen und die Werte auszugeben\n");

		int[] array = new int[10];
		int n = 1;
		
		for (int i = 0; i < array.length; i++) {
			array[i] = n;
			n += 2;
		}
		
		for (int i = 0; i < array.length; i++) {
			System.out.println("Array Index " + i + " hat den Wert " + array[i]);
			
		}
	}

}
