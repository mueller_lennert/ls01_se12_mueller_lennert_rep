package abArray2;

public class ArrayHelper {

	public static void main(String[] args) {

		int[] zahlen = {10, 25, 50, 125, 625};
		String zahlenArray = convertArrayToString(zahlen);
		System.out.println(zahlenArray);

	}
	
	public static String convertArrayToString(int[] zahlen) {
		String zahlenArray = "";
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlenArray += zahlen[i];
			if (i < zahlen.length-1) {
				zahlenArray += ",";
			}
		}
		return zahlenArray;
	}

}
