package abArray2;
public class ArrayUmdrehen {
	
	
	public static void main(String[] args) {
		
		int[] array = new int[(int) (Math.random() * 9)];
		
		for (int i = 0; i < array.length; i++) {
			array[i] = (int) (Math.random() * 150);
		}
		
		System.out.println("Orginales Array: ");
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		System.out.print("\n");
		
		System.out.print("Umgedrehtes Array: ");
		int[] array1 = arrayEinzelnUmdrehen(array);
		System.out.print("\n");
		for (int i = 0; i < array1.length; i++) {
			System.out.println(array1[i]);
		}
		
		System.out.println("\nUmgedrehtes Array(2): ");
		int[] array2 = arrayUmdrehen(array);
		for (int i = 0; i < array2.length; i++) {
		System.out.println("Wert bei Array Index " + i + " ist: " + array2[i]);
		if (array[i] == ';') {
			System.out.print("\n");
			}	
		}
		System.out.print("\n");	
		
	}
	
	public static int[] arrayEinzelnUmdrehen(int[] array) {
		
		int j = 0, n = array.length;
		for (int i = 0; i < n / 2; i++) {
	           j = array[i];
	            array[i] = array[n - i - 1];
	            array[n - i - 1] = j;
			
		}
		
		return array;
	}

	public static int[] arrayUmdrehen(int[] array) {
		
		int j = array.length, n = array.length;
		int[] tempArray = new int[array.length];
		for (int i = 0; i < n; i++) {
	        tempArray[j - 1] = array[i];
            j = j - 1;
			}
		return array;
	}
}

