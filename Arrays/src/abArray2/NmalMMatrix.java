package abArray2;

import java.util.Scanner;

public class NmalMMatrix {

	public static void main(String[] args) {

		System.out.println("Hinweis: Dieses Programm erstellt eine NxM Matrix mit\nzufälligen Werten und gibt die Werte der Matrix aus.");
		System.out.print("\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Wie lang soll ihre Matrix sein? ");
		int dimension1 = sc.nextInt();
		System.out.print("Wie breit soll ihre Matrix sein? ");
		int dimension2 = sc.nextInt();
		
		int[][] matrix = new int[dimension1][dimension2];
		int[] werte = new int[(dimension1 * dimension2)];
        for (int i = 0; i < werte.length; i++) {
		werte[i] = (int) Math.abs(Math.random() * 150);
        }

		String matrixWerte = matrixLesen(matrix, werte);
		System.out.print(matrixWerte);

		System.out.print("\n");
		sc.close();

	}
	
	public static String matrixLesen(int[][] matrix, int[] werte) {
		
		String matrixWerte = "";
		int arrayIndex = 0;
		for (int i = 0; i <= matrix.length-1; i++) {
			for (int j = 0; j <= matrix[0].length-1; j++) {
				matrix[i][j] = werte[arrayIndex];
				arrayIndex++;
			}
		}
		
		for (int i = 0; i <= matrix.length-1; i++) {
			for (int j = 0; j <= matrix[0].length-1; j++) {
				matrixWerte += matrix[i][j] + " ";
			}
			matrixWerte += "\n";
		}
		
		return matrixWerte;	
	}
}
