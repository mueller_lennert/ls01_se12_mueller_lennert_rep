package abArray2;

import java.util.Scanner;

public class NmalNMatrix {

	public static void main(String[] args) {

		System.out.println("Hinweis: Dieses Programm erstellt eine 3x3 Matrix und �berpr�ft ob diese zu sich selbst transponiert ist.");
		System.out.print("\n");
		
		Scanner sc = new Scanner(System.in);
		int[][] matrix = new int[3][3];
		matrix = matrixBerechnen(matrix);
		for (int i = 0; i <= 2; i++) {
			for (int j = 0; j <= 2; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		sc.close();
		
		for (int i = 1; i <= 2; ++i) {
			for (int j = 1; j <= 3; ++j) {
				if (matrix[i-1][j-1] == matrix[j-1][i-1] && i != j && (i!= 0 && j != 1)) {
					System.out.println("Bei Punkt " + i + " " + j + " und " + j + " " + i + " ist die Matrix zu sich selbst transponiert.       Wert 1: " + matrix[i-1][j-1] + " Wert 2: " + matrix[j-1][i-1]);
				}
				
				else if (matrix[i-1][j-1] != matrix[j-1][i-1] && i != j) {
					System.out.println("Bei Punkt " + i + " " + j + " und " + j + " " + i + " ist die Matrix nicht zu sich selbst transponiert. Wert 1: " + matrix[i-1][j-1] + " Wert 2: " + matrix[j-1][i-1]);
				}
			}
		}
	}
	
	public static int[][] matrixBerechnen(int[][] matrix) {
		
		for (int i = 1; i <= 3; ++i) {
			for (int j = 1; j <= 3; ++j) {
				matrix[i-1][j-1] = i * j;
			}
		}
		
		//Hier wird ein Fehler in die Matrix eingebaut
		matrix[1][2] = 7;
		
		return matrix;
	}
}
