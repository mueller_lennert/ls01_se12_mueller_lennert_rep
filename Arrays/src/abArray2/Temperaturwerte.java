package abArray2;

import java.util.Scanner;

public class Temperaturwerte {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Dieses Programm benutzt ein zweidimensionales Array,\num eine Temperaturtabelle zu berechnen und auszugeben\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Wie gro� soll das Array sein: ");
		int arrayGroesse = sc.nextInt();
		double[][] temperatur = new double[arrayGroesse][2];
		temperatur = temperaturBerechnen(temperatur);
		for (int i = 0; i < temperatur.length; i++) {
			System.out.printf("%.2f�F %.2f�C\n", temperatur[i][0], temperatur[i][1]);
		}
		sc.close();
	}
	
	public static double[][] temperaturBerechnen(double[][] temperatur) {
		
		int j = 0;
		for (int i = 0; i < temperatur.length; i++) {
			temperatur[i][0] = j;
			j += 10;
			temperatur[i][1] = (temperatur[i][0] - 32.0) * (5.0/9.0); 
		}
		
		return temperatur;
	}

}
