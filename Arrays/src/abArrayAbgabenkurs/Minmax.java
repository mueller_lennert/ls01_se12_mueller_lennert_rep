package abArrayAbgabenkurs;

public class Minmax {
	
	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm erstellt 20 zuf�llige Zahlen zwischen\n1 bis 1000 und gibt diese mit der Indexnummer aus.\nDanach wird die gr��te und die kleinste Zahl gesucht\n");
		
		final int[] zahlen = new int[20];
		int kleinsteZahl, groessteZahl;
		byte kleinsteZahlIndex = 0, groessteZahlIndex = 0;
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = (int) (Math.random() * 1000) + 1;
		}
		
		for (int i = 0; i < zahlen.length; i++) {
			System.out.printf("Zahl [%-2d]  = %-5d", i, zahlen[i]);
			if ((i+1) % 4 == 0) {
				System.out.print("\n");
			}
		}
		
		kleinsteZahl = zahlen[0];
		groessteZahl = zahlen[0];
		for (byte i = 1; i < zahlen.length; i++) {
			if (kleinsteZahl > zahlen[i]) {
				kleinsteZahl = zahlen[i];
				kleinsteZahlIndex = i;
			}
			if (groessteZahl < zahlen[i]) {
				groessteZahl = zahlen[i];
				groessteZahlIndex = i;
			}
				
		}
		
		System.out.println("\nDie kleinste Zahl ist " + kleinsteZahl + " bei Position: " + kleinsteZahlIndex);
		System.out.println("Die gr��te Zahl ist " + groessteZahl + " bei Position: " + groessteZahlIndex );
	}
}
