package abArrayAbgabenkurs;

import java.util.Scanner;

public class MyArray {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		//Aufgabe 1: Array mit 10 Stellen erstellen
		int[] myArray = new int[10];
		
		//Aufgabe 2: Es wird der Array Index + 1 als Wert gespeichert
		//Aufgabe 3: Die for-Schleife gibt die 10 Werte aus
		for (int i = 0; i < myArray.length; i++) {
			myArray[i] = i+1;
			System.out.println("Wert bei Index " + i + " ist: " + myArray[i]);
		}
		
		//Aufgabe 4: Bei Index 3 wird der Wert auf 1000 gesetzt
		System.out.println("\nWert von myArray[3] auf 1000 gesetzt.");
		myArray[3] = 1000;
		
		//Aufgabe 5: Der Wert von myArray bei Index 3 wird ausgegeben
		System.out.println("Wert von myArray[3] ist: " + myArray[3]);
		
		//Aufgabe 6: Alle Werte des Array werden auf 0 gesetzt
		for (int i = 0; i < myArray.length; i++) {
			myArray[i] = 0;
		}	
		
		char wiederholen = ' ';
		do {
		//Aufgabe 7: Das Men� wird ausgegeben
		System.out.println("\nW�hlen Sie eins der folgenden Punkte:");
		System.out.println("a. Alle Werte ausgeben");
		System.out.println("b. Einen bestimmten Wert ausgeben");
		System.out.println("c. Das Array komplett mit neuen Werten bef�llen");
		System.out.println("d. Einen bestimmten Wert �ndern");
		char auswahl = ' ';
		auswahl = sc.next().charAt(0);
		System.out.print("\n");
		
		switch (auswahl) {
		//a. Alle Werte ausgeben
		case 'a': 
		for (int i = 0; i < myArray.length; i++) {
			System.out.println("Wert bei Index " + i + " ist: " + myArray[i]);
		}
		System.out.print("\n");
		break;
		
		//b. Einen bestimmten Wert ausgeben
		case 'b': 
		byte index = 0;
		System.out.print("Von welchem Array Index soll der Wert ausgebgeben werden? ");
		index = sc.nextByte();
		System.out.println("Wert bei Index " + index + " ist: " + myArray[index] + "\n");
		System.out.print("\n");
		break;
		
		//c. Das Array komplett mit neuen Werten bef�llen
		case 'c': 
		for (int i = 0; i < myArray.length; i++) {
			System.out.print("Legen Sie den Wert f�r Array Index " + i + " fest. Aktueller Wert: " + myArray[i] + ", Neuer Wert: ");
			myArray[i] = sc.nextInt();
		}	
		System.out.print("\n");
		break;
		
		//d. Einen bestimmten Wert �ndern"
		case 'd':
		System.out.print("Von welchem Array Index soll der Wert ge�ndert werden? ");
		index = sc.nextByte();
		System.out.println("Aktueller Wert: " + myArray[index]);
		System.out.print("Geben Sie den neuen Wert ein: ");
		myArray[index] = sc.nextInt();
		System.out.print("\n");
		break;
		}
		
		System.out.print("M�chten sie noch eine Auswahl t�tigen? ");
		System.out.println("Geben Sie \"N\" zum abbrechen ein. ");
		System.out.print("Geben Sie ein anderes Zeichen zum Fortfahren ein. ");
		wiederholen = sc.next().charAt(0);
		//Aufgabe 8: Mithilfe einer do-while Schleife, wird das Programm
		//solange wiederholt, bis der Nutzer dies abbricht.
		} while (wiederholen != 'N' || wiederholen != 'n');

		sc.close();
	}

}
