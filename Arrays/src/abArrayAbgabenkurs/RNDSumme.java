package abArrayAbgabenkurs;

public class RNDSumme {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm \"würfelt\" 10000 mal und gibt aus,\nwie oft dabei welche Zahl gewürfelt wurde.\n");
		
		final byte[] würfelZahlen = new byte[10000];
		short[] hauefigkeit = {0,0,0,0,0,0};
		
		for (short i = 0; i < würfelZahlen.length; i++) {
			würfelZahlen[i] = (byte) ((byte) (Math.random() * 6) + 1);
		}
		
		/*for (short i = 0; i < würfelZahlen.length; i++) {
			System.out.printf("Zahl [%-2d]  = %-5d", i, würfelZahlen[i]);
			if ((i+1) % 4 == 0) {
				System.out.print("\n");
			}
		}*/
		
		for (short i = 0; i < würfelZahlen.length; i++) {
			switch (würfelZahlen[i]) {
			case 1: hauefigkeit[0]++; break;
			case 2: hauefigkeit[1]++; break;
			case 3: hauefigkeit[2]++; break;
			case 4: hauefigkeit[3]++; break;
			case 5: hauefigkeit[4]++; break;
			case 6: hauefigkeit[5]++; break;
			}
		}
		
		for (short i = 0; i < hauefigkeit.length; i++) {
			System.out.println("Die Zahl " + (i+1) + " kam " + hauefigkeit[i] + " mal vor.");
		}

	}

}
