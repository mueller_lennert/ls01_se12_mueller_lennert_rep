package abArrayAbgabenkurs;

public class Square {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm f�llt eine Liste mit Zahlen von 1 bis 10\nund rechnet die jeweilige Quadratszahl aus.\n");
		
		int[] zahlen = new int[10];
		int[] quadratzahlen = new int[10];
		
		for (byte i = 0; i < zahlen.length; i++) {
			zahlen[i] = i+1;
			quadratzahlen[i] = (int) Math.pow(zahlen[i], 2);
		}
		
		for (byte i = 0; i < zahlen.length; i++) {
			System.out.println("Index " + i + ": Die Quadratzahl von " + zahlen[i] + " ist " + quadratzahlen[i]);
		}

	}

}
