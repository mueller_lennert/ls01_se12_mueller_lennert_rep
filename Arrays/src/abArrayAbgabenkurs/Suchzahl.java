package abArrayAbgabenkurs;

import java.util.Scanner;

public class Suchzahl {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm erstellt 100 zuf�llige Zahlen zwischen\n1 bis 25 und gibt diese mit der Indexnummer aus.\n");
	
		Scanner sc = new Scanner(System.in);
		
		final int[] zahlen = new int[100];
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = (int) (Math.random() * 25) + 1;
		}
		
		for (int i = 0; i < zahlen.length; i++) {
			System.out.printf("Zahl [%-2d]  = %-5d", i, zahlen[i]);
			if ((i+1) % 4 == 0) {
				System.out.print("\n");
			}
		}
		
		System.out.print("\nNach welcher Zahl soll gesucht werden? ");
		int key = sc.nextInt();
		boolean gefunden = false;
		byte anzahlGefunden = 0;
		
		for (int i = 0; i < zahlen.length; i++) {
			if (key == zahlen[i]) {
				System.out.println("Die Zahl " + key + " wurde bei Index: " + i + " gefunden.");
				gefunden = true;
				anzahlGefunden++;
			}
		}
		
		if (gefunden == true) {
			System.out.print("Zahl " + key + " wurde insgesamt " + anzahlGefunden + " mal gefunden.");
		}
		
		else if (gefunden == false) {
			System.out.print("Die Zahl " + key + " kommt nicht im Array vor.");
		}
		
		sc.close();
	}

}
