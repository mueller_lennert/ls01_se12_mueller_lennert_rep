package abArrayAbgabenkurs;

public class Zufallszahlen {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm erstellt 20 zuf�llige Zahlen zwischen\n1 bis 10 und gibt diese mit der Indexnummer aus.\n");
	
		float[] zahlen = new float[20];
		
		for (byte i = 0; i < zahlen.length; i++) {
			zahlen[i] = (float) (Math.random() * 10);
		}
		
		for (byte i = 0; i < 10; i++) {
			System.out.println("Zahl [" + i + "]  = " + zahlen[i]);
		}
		
		for (byte i = 10; i < 20; i++) {
			System.out.println("Zahl [" + i + "] = " + zahlen[i]);
		}

	}

}
