package ls81.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author dariush
 *
 */
public class CSVHandler {

  /**
   * muss sich im aktuellen Ordner befinden!
   */
  private String file;
  private String delimiter;
  private String line = "";
  private HashMap<String, Integer> spaltenZielSortierung = new HashMap<>();
  private HashMap<Integer, String> spaltenEingabeSortierung = new HashMap<>();

  /**
   * Default constructor
   */
  public CSVHandler() {
    this(";", "studentNameCSV.csv");
  }

  /**
   * Standard constructor
   * 
   * @param delimiter, Trennzeichen
   * @param file,      Datei zum Einlesen
   */
  // Constructor 2
  public CSVHandler(String delimiter, String file) {
    super();
    this.delimiter = delimiter;
    this.file = file;
  }

  /**
   * Liest alle Sch�ler aus der csv aus und gibt sie zur�ck
   * 
   * @return List mit Sch�lern
   */
  public List<Schueler> getAll() {
    Schueler s = null;
    List<Schueler> students = new ArrayList<Schueler>();

    try {
    	 BufferedReader reader = new BufferedReader(new FileReader(file));
         String[] standardSortierung = {"Vorname", "Name", "joker", "blamiert", "fragen"};
         
         line = reader.readLine();
         if (!line.equals(null)) {
             String[] spaltenUnsortiert = line.split(delimiter);
             for (int i = 0; i < standardSortierung.length; i++) {
            	 for (int j = 0; j < spaltenUnsortiert.length; j++) {
            		 if (spaltenUnsortiert[j].equals(standardSortierung[i]))
            			 spaltenZielSortierung.put(spaltenUnsortiert[j], i);
            	 }
            	 spaltenEingabeSortierung.put(i, spaltenUnsortiert[i]);
             }
             String[] spalte = new String[standardSortierung.length];
             for (int i = 0; i < spalte.length; i++) {
            	 String aktuelleSpalte = spaltenUnsortiert[i];
            	 int richtigerIndex = spaltenZielSortierung.get(aktuelleSpalte);
            	 //System.out.println("I: " + i + " Richtiger Index: " + richtigerIndex);
            	 spalte[i] = spaltenUnsortiert[richtigerIndex];
            	 //System.out.println(spaltenUnsortiert[i] + " \n " + spalte[i]);
             }
             
        	 String spalte2Gro� = spalte[2].substring(0, 1).toUpperCase() + spalte[2].substring(1);
        	 String spalte3Gro� = spalte[3].substring(0, 1).toUpperCase() + spalte[3].substring(1); 
        	 String spalte4Gro� = spalte[4].substring(0, 1).toUpperCase() + spalte[4].substring(1); 
        	 System.out.printf("%-15s %-8s %-14s %-10s\n", spalte[1], spalte2Gro�, spalte3Gro�, spalte4Gro�.replace("Fragen", "Frage"));
         } else {
        	 System.out.println("ERROR: Die erste Zeile konnte nicht ausgewertet werden");
        	 System.exit(2);
         }
         //--------Hier Debug Ausgabe---------
         System.out.println("**Eingabesortierung: " + spaltenEingabeSortierung.toString() + "\n**Zielsortierung: " + spaltenZielSortierung.toString());         
         
         while ((line = reader.readLine()) != null) {    	 
            
        	 String[] spaltenUnsortiert = line.split(delimiter);
        	 String[] spalte = new String[spaltenUnsortiert.length];
        	 for (int i = 0; i < spaltenZielSortierung.size(); i++) {
        		 String aktuelleSpalte = spaltenEingabeSortierung.get(i);
        		 //System.out.println(aktuelleSpalte + " -- " + i + " -- " + spaltenZielSortierung.get(aktuelleSpalte));
        		 int richtigerIndex = spaltenZielSortierung.get(aktuelleSpalte);
        		 //System.out.println("I: " + i + " Richtiger Index: " + richtigerIndex);
        		 //if (!spaltenUnsortiert.equals(null))
        		 spalte[i] = spaltenUnsortiert[richtigerIndex];
        		 //System.out.println(aktuelleSpalte + " .-. " + spalte[i] + "\n ");
        	}  
            
            System.out.printf("%-15s %-8s %-14s %-10s\n", spalte[0] + " " + spalte[1], spalte[2], spalte[3], spalte[4]);
            s = new Schueler(spalte[0] + " " + spalte[1], Integer.parseInt(spalte[2]), Integer.parseInt(spalte[3]), Integer.parseInt(spalte[4]));
            students.add(s);
         }
         System.out.print("\n");
         reader.close();

    } catch (IOException e) {

      e.printStackTrace();

    }
    
  	catch (ArrayIndexOutOfBoundsException | NullPointerException e2) {
  		e2.printStackTrace();
  	}

    return students;
  }

  /**
   * Gibt alle Sch�ler aus
   * 
   * @param students
   */
  public void printAll(List<Schueler> students) {
  	System.out.printf("%32s\n", "Liste der Sch�ler:");
    for (Schueler s : students) {
    	System.out.printf("%-15s %-8s %-14s %-10s\n", s.getName(), s.getJoker(), s.getBlamiert(), s.getFragen());
    	}
  	}
}
