package Auftrag1;

import javax.xml.parsers.*;
import org.w3c.dom.*;


public class ReadBookstoreData1 {

  public static void main(String[] args) {

	  	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	try {
    		DocumentBuilder dbuilder = factory.newDocumentBuilder();
    		Document doc = dbuilder.parse("src/Auftrag1/buchhandlung.xml");
    	
    		NodeList titelList = doc.getElementsByTagName("titel");
    		for (short i = 0; i < titelList.getLength(); i++) {
    			Node titelNode = titelList.item(i);
    			System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    } 
  }
}
