package Auftrag2;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* Arbeitsauftrag:  Lesen Sie den Titel und den Autor des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                        titel:  Java ist auch eine Insel 
 *                        autor:  Max Mustermann 
 */



public class ReadBookstoreData2 {

	public static void main(String[] args) {

	  	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	try {
    		DocumentBuilder dbuilder = factory.newDocumentBuilder();
    		Document doc = dbuilder.parse("src/Auftrag2/buchhandlung.xml");
    	
    		NodeList titelList = doc.getElementsByTagName("titel");
    		for (short i = 0; i < titelList.getLength(); i++) {
    			Node titelNode = titelList.item(i);
    			System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());
    		}
    		
    		NodeList authorList = doc.getElementsByTagName("autor");
    		for (int i = 0; i < authorList.getLength(); i++) {
    			Node authorNode = authorList.item(i);
    			System.out.println(authorNode.getNodeName() + ": " + authorNode.getTextContent());
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    } 
  }
}
