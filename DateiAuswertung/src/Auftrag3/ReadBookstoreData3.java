package Auftrag3;

 /* Arbeitsauftrag:  Lesen Sie alle Angaben des Buches "Java ist auch eine Insel"
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                    titel:  Java ist auch eine Insel   
 *					  vorname:  Christian 
 *                    nachname:  Ullenboom 
 */

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData3 {

	public static void main(String[] args) {

	  	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	try {
    		DocumentBuilder dbuilder = factory.newDocumentBuilder();
    		Document doc = dbuilder.parse("src/Auftrag3/buchhandlung.xml");
    	
    		NodeList titelList = doc.getElementsByTagName("titel");
    		for (short i = 0; i < titelList.getLength(); i++) {
    			Node titelNode = titelList.item(i);
    			System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());
    		}
    		
    		NodeList autorVornamen = doc.getElementsByTagName("vorname");
    		for (int i = 0; i < autorVornamen.getLength(); i++) {
    			Node autorVorname = autorVornamen.item(i);
    			System.out.println(autorVorname.getNodeName() + ": " + autorVorname.getTextContent());
    		}
    		
    		NodeList autorNachnamen = doc.getElementsByTagName("nachname");
    		for (int i = 0; i < autorNachnamen.getLength(); i++) {
    			Node autorNachname = autorNachnamen.item(i);
    			System.out.println(autorNachname.getNodeName() + ": " + autorNachname.getTextContent());
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    } 
  }
}
