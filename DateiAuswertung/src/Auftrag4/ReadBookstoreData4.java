package Auftrag4;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* Arbeitsauftrag:  Lesen Sie nur die Buchtitel 
 *					aus der Datei "buchhandlung.xml" und geben Sie sie 
 * 					auf dem Bildschirm aus.
 * 
 *                  Ausgabe soll wie folgt formatiert werden:
 *                     1. titel: Everyday Italian
 *                     2. titel: Harry Potter
 *                     3. titel: XQuery Kick Start
 *                     4. titel: Learning XML
 *                     
 * Hinweis: Sie benötigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */



public class ReadBookstoreData4 {

	public static void main(String[] args) {

	  	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	try {
    		DocumentBuilder dbuilder = factory.newDocumentBuilder();
    		Document doc = dbuilder.parse("src/Auftrag4/buchhandlung.xml");
    	
    		NodeList titelList = doc.getElementsByTagName("titel");
    		for (short i = 0; i < titelList.getLength(); i++) {
    			Node titelNode = titelList.item(i);
    			System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    } 
  }
}
