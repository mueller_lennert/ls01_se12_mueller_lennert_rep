package Auftrag5;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* Arbeitsauftrag:  Lesen Sie nur die Autoren des Buches "XQuery Kick Start" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  
 *                   Ausgabe soll wie folgt aussehen:
 *                     Buchtitel:  XQuery Kick Start
 *                     Autoren: 
 *                     	    1. autor: James McGovern
 *                          2. autor: Per Bothner
 *                          3. autor: Kurt Cagle
 *                          4. autor: James Linn
 *                          5. autor: Vaidyanathan Nagarajan
 *                          
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */

public class ReadBookstoreData5 {

	public static void main(String[] args) {

	  	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	try {
    		DocumentBuilder dbuilder = factory.newDocumentBuilder();
    		Document doc = dbuilder.parse("src/Auftrag5/buchhandlung.xml");
    		
    		NodeList buchListe = doc.getElementsByTagName("buch");
            if (buchListe.getLength() > 0) {
                for (short i = 0; i < buchListe.getLength(); i++) {
                    Node buch = buchListe.item(i);

                    Node titelElement = ((Element) buch).getElementsByTagName("titel").item(0);
                    String buchtitel = titelElement.getTextContent();
                    System.out.println("Buchtitel:  " + buchtitel);

                    NodeList autorListe = ((Element) buch).getElementsByTagName("autor");
                    if (autorListe.getLength() > 0) {
                        System.out.println("Autoren:");
                        // Ausgabe der Autoren
                        for (short j = 0; j < autorListe.getLength(); j++) {
                            Node autorElement = autorListe.item(j);
                            String autor = autorElement.getTextContent();
                            System.out.printf("%s\n", "            " + (j + 1) + ". autor: " + autor);
                        }
                    } else {
                        System.out.println("ERROR: Keine Autoren gefunden");
                    }
                    System.out.println();
                }
            } else {
                System.out.println("ERROR: Keine B�cher gefunden");
            }
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    } 
  }
}
