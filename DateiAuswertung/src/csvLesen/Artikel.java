package csvLesen;

public class Artikel {
	String artikelName;
	float preis;
		  
	public Artikel() {
		this.artikelName = "";
		this.preis = -1f;
	}
		
	public Artikel(String artikelName, float preis) {
		this.artikelName = artikelName;
		this.preis = preis;
	}
		  
	public String getArtikelName() {
	    return artikelName;
	}

	public void setArtikelName(String artikelName) {
	    this.artikelName = artikelName;
	}

	public float getPreis() {
	    return preis;
	}

	public void setPreis(float preis) {
	    this.preis = preis;
	}
}
