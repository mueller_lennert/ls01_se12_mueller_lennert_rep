package csvLesen;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class CSVDateiLesenBeispiel {

@SuppressWarnings("unused")
public static void main(String[] args) {
    
    try {
      String fileLocation = System.getProperty("user.home"); 
      BufferedReader reader = new BufferedReader(new FileReader(fileLocation + "\\Documents\\Ausbildung\\Berufsschule\\2023-05-15\\artikelliste.csv"));
      String text = "", neueZeile = "";
      Vector<Artikel> artikelListe = new Vector<Artikel>();
      boolean tabellenkopfLesen = true;
      
      //while ( (zeile = reader.readLine()) != null ) {  
      while ( (neueZeile = reader.readLine()) != null ) {  
    	  //if (zeile.contains("Maus") != false) {
    	  	text += neueZeile + "\n"; 
    	      String[] spalte = neueZeile.split(";");
    	    try {
    	    if (tabellenkopfLesen) {
    	    	System.out.printf("%-15s %-10s\n", spalte[0], spalte[1]);
    	    	tabellenkopfLesen = false;
    	    }
    	    
    	    else {	
    	    	float preis;
    	    	try {
    	    		preis = Float.valueOf(spalte[1].replace(",", "."));
    	    	} catch (java.lang.NumberFormatException e) {
    	    		preis = Float.NaN;
    	    		System.out.println("ERROR: Artikelpreis konnte f�r " + spalte[0] + " nicht ermittelt werden");
    	    	}
    	    
    	    	Artikel artikel = new Artikel(spalte[0], preis);
    	    	artikelListe.addElement(artikel);
    	    	System.out.printf("%-15s %-10.2f\n", artikel.getArtikelName(), artikel.getPreis());
    	    	}
    	    } catch (java.lang.ArrayIndexOutOfBoundsException e) { }

    	  //}
      } // end of while
      
      reader.close();
      
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    	} 
  	}
}

