package xmlSchreiben;

import java.io.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.w3c.dom.*;

public class WriteBookstoreDatei {

	public static void main(String[] args) {
		
	  	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	try {
    		DocumentBuilder dbuilder = factory.newDocumentBuilder();
    		Document doc = dbuilder.newDocument();

    		Element buchhandlung = doc.createElement("buchhandlung");
    		doc.appendChild(buchhandlung);
    		
    		Element buch = doc.createElement("buch");
    		buch.setAttribute("lang", "de");
    		buch.appendChild(doc.createTextNode("Einführung in OOP"));
    		
    		buchhandlung.appendChild(buch);
    		
    		//XML-Datei schreiben
    		
    		TransformerFactory transformerFactory = TransformerFactory.newInstance();
    		Transformer transformer = transformerFactory.newTransformer();
    		DOMSource source = new DOMSource(doc);
    		
    		StreamResult result = new StreamResult(new File("src/xmlSchreiben/buchhandlung.xml"));
    		
    		transformer.transform(source, result);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    	} 
	}

}
