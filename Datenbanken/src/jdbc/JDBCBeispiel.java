package jdbc;
import java.sql.*;

public class JDBCBeispiel {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            //Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mitarbeiterdb", "root", "");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/versand", "root", "");
            Statement st = con.createStatement();

            //ResultSet rs = st.executeQuery("SELECT * FROM T_Mitarbeiter");
            ResultSet rs = st.executeQuery("SELECT * FROM T_Artikel;");

            while (rs.next()) {
                System.out.println(rs.getString("bezeichnung"));
            }
            
            /*		
            st.executeUpdate("DELETE FROM T_mitarbeiter WHERE MitarbeiterNr = 1002");
            st.executeUpdate("INSERT INTO t_mitarbeiter  VALUES (1002,'Anna','toi')");
            
            String strPwd="neuesPasswort";
            st.executeUpdate("UPDATE T_Mitarbeiter SET pwd='"+strPwd+"' WHERE MitarbeiterNr = 1001");
            
            rs = st.executeQuery("SELECT * FROM T_Mitarbeiter");
            while (rs.next()) {
                System.out.println(rs.getString("Name") + " , " + rs.getString("pwd"));
            }*/
            con.close();
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
