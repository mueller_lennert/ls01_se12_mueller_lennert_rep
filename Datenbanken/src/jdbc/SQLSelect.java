package jdbc;
import java.sql.*;
import java.util.Scanner;

public class SQLSelect {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            Scanner sc = new Scanner(System.in);
            
            /*System.out.print("Gebe den Benutzernamen f�r die Datebankverbindung ein: ");
            String benutzer = sc.next();
            System.out.print("Gebe das Passwort f�r die Datebankverbindung ein:");
            String passwort = sc.nextLine();
            System.out.print("\n");*/
            
            //Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/versand", benutzer, passwort);
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/versand", "root", "");
            Statement st = con.createStatement();
            
            //ResultSet rs = st.executeQuery("SELECT * FROM T_Mitarbeiter");
            //ResultSet rs = st.executeQuery("SELECT * FROM T_Artikel");
            String name = "";
            while (!name.equals("abbrechen")) {
            System.out.print("Nach einem Kunden suchen: ");
            name = sc.next();
            ResultSet rs = st.executeQuery("SELECT * FROM T_Kunden WHERE nname = '" + name + "'");

            while (rs.next()) {
                System.out.println(rs.getString("vname") + " " + rs.getString("nname") + " ist vorhanden.");
            }
            System.out.print("\n");
            }
            
            /*		
            st.executeUpdate("DELETE FROM T_mitarbeiter WHERE MitarbeiterNr = 1002");
            st.executeUpdate("INSERT INTO t_mitarbeiter  VALUES (1002,'Anna','toi')");
            
            String strPwd="neuesPasswort";
            st.executeUpdate("UPDATE T_Mitarbeiter SET pwd='"+strPwd+"' WHERE MitarbeiterNr = 1001");
            
            rs = st.executeQuery("SELECT * FROM T_Mitarbeiter");
            while (rs.next()) {
                System.out.println(rs.getString("Name") + " , " + rs.getString("pwd"));
            }*/

            sc.close();
            con.close();
            
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }

    }
}
