/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package versandgui;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author A. Hafezi
 */
public class Adresse {

    private String str;
    private int plz;
    private String ort;

    public Adresse(){
        str = "";
        plz = 0;
        ort = "";
    }

    public Adresse(String str, int plz, String ort){
        this.str = str;
        this.plz = plz;
        this.ort = ort;
    }

    public void setStr(String str){
        this.str = str;
    }
    public String getstr(){
        return this.str;
    }

    public void setPlz(int plz){
        this.plz = plz;
    }
    public int getPlz(){
        return this.plz;
    }

    public void setOrt(String ort){
        this.ort = ort;
    }
    public String getOrt(){
        return this.ort;
    }
}
