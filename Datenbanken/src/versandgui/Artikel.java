package versandgui;

public class Artikel {
	
	String p_artikel_id;
	String lagerplatz;
	double listenpreis;
	String bezeichnung;
	int bestand;
	int mindestBestand;
	String verpackung;
	int f_mwstNr;
	
	public Artikel() {
		
		this.p_artikel_id = "0";
		this.lagerplatz = "0";
		this.listenpreis = 0.0;
		this.bezeichnung = "no_name";
		this.bestand = 0;
		this.mindestBestand = 0;
		this.verpackung = "0";
		this.f_mwstNr = -1;
	}
	
	//Parametrisierter Konstruktor
	public Artikel(String p_artikel_id, String lagerplatz, double listenpreis, 
			String bezeichnung, int bestand, int mindestBestand, String verpackung, int f_mwstNr) {
	    this.p_artikel_id = p_artikel_id;
	    this.lagerplatz = lagerplatz;
	    this.listenpreis = listenpreis;
	    this.bezeichnung = bezeichnung;
	    this.bestand = bestand;
	    this.mindestBestand = mindestBestand;
	    this.verpackung = verpackung;
	    this.f_mwstNr = f_mwstNr;
	}
	
	//Verwaltungmethoden mit Setter und Getter
	public String get_artikel_id() {
	    return p_artikel_id;
	}

	public void set_artikel_id(String artikel_id) {
	    this.p_artikel_id = artikel_id;
	}

	public String getLagerplatz() {
	    return lagerplatz;
	}

	public void setLagerplatz(String lagerplatz) {
	    this.lagerplatz = lagerplatz;
	}

	public double getListenpreis() {
	    return listenpreis;
	}

	public void setListenpreis(double listenpreis) {
	    this.listenpreis = listenpreis;
	}

	public String getBezeichnung() {
	    return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
	    this.bezeichnung = bezeichnung;
	}

	public int getBestand() {
	    return bestand;
	}

	public void setBestand(int bestand) {
	    this.bestand = bestand;
	}

	public int getMindestBestand() {
	    return mindestBestand;
	}

	public void setMindestBestand(int mindestBestand) {
	    this.mindestBestand = mindestBestand;
	}

	public String getVerpackung() {
	    return verpackung;
	}

	public void setVerpackung(String verpackung) {
	    this.verpackung = verpackung;
	}

	public int get_mwstNr() {
	    return f_mwstNr;
	}

	public void set_mwstNr(int mwstNr) {
	    this.f_mwstNr = mwstNr;
	}

}
