//package versandschulegui;
package versandgui;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ArtikelAnzeigenGUI extends JFrame {

private static final long serialVersionUID = 1L;
private JButton jbtnSchliessen = new JButton();
private JTextArea jtarArtikel [] = null;
private JLabel jlblInfo = new JLabel();

  public ArtikelAnzeigenGUI(String title, LinkedList<?> liste) {

      configFrame(title);
      toFront();

      int anzahlArtikel = liste.size();
      //getContentPane().setBackground(Color.);

      //--- Hier Komponenten erzeugen und mit getContentPane().add()
      //--- in das Fenster einf�gen
      Container panel = null;
      panel = getContentPane();

      LayoutManager layout;
      layout = new GridLayout (anzahlArtikel+2,1);

      panel.setLayout (layout);
      panel.setBackground (Color.LIGHT_GRAY);

      //--- Anfang Komponenten
      jlblInfo.setText("Es befinden sich "+anzahlArtikel + " Artikel in der Datenbank");
      panel.add(jlblInfo);
      String ausgabe = "";
      Artikel artikelaus = new Artikel();
      jtarArtikel = new JTextArea[anzahlArtikel];
      Font font = new Font(Font.MONOSPACED, Font.PLAIN, 13);
      if (!liste.isEmpty()) {
        for (int i = 0; i < anzahlArtikel; i++) {
          jtarArtikel[i] = new JTextArea();
          jtarArtikel[i].setFont(font);
          artikelaus = (Artikel) liste.get(i);
          ausgabe = String.format("%-7s", artikelaus.get_artikel_id());
          ausgabe += String.format("%-3s", artikelaus.getLagerplatz());
          ausgabe += String.format("%-7s", artikelaus.getListenpreis());
          ausgabe += String.format("%-30s", artikelaus.getBezeichnung());
          ausgabe += String.format("%-5s", artikelaus.getBestand());
          ausgabe += String.format("%-5s", artikelaus.getMindestBestand());
          ausgabe += String.format("%-15s", artikelaus.getVerpackung());
          ausgabe += String.format("%-5s", artikelaus.get_mwstNr());
          jtarArtikel[i].setText(ausgabe);
          panel.add(jtarArtikel[i]);
        }
      }

      jbtnSchliessen.setText("Schliessen");
      jbtnSchliessen.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              jbtnSchliessen_ActionPerformed(evt);
          }
      });
      panel.add(jbtnSchliessen);

      //--- Ende Komponenten

      setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setResizable(false);
      setVisible(true);
  }
  private void jbtnSchliessen_ActionPerformed(ActionEvent evt) {
    setVisible(false);
    dispose();
  }

  private void configFrame(String title){
    this.setTitle(title);
    int frameWidth  = 700;
    int frameHeight = 400;
    this.setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width  - getSize().width)/2;
    int y = (d.height - getSize().height)/2;
    this.setLocation(x+100,y+100);
    Font font = new Font(Font.MONOSPACED, Font.PLAIN, 12);
    this.setFont(font);
  }

}
