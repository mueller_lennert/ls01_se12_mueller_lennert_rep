/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//package versandschulegui;
package versandgui;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 *
 * @author goekcen
 */
public class ArtikelGUI extends JFrame{

   private static final long serialVersionUID = 1L;
   private JButton jbtnAlleAnzeigen = new JButton();
   private JButton jbtnZurueck = new JButton();
   private VersandVerwaltung verw;


   public ArtikelGUI (String title, VersandVerwaltung verwalt) {
      //--- Hauptfenster konfigurieren
      configFrame(title);
      toFront();

      //--- Verbindung zur DB
      verw = verwalt;

      //getContentPane().setBackground(Color.);

      //--- Hier Komponenten erzeugen und mit getContentPane().add()
      //--- in das Fenster einf�gen
      Container panel = null;
      panel = getContentPane ();

      LayoutManager layout;
      layout = new GridLayout (4,1);

      panel.setLayout (layout);
      panel.setBackground (Color.LIGHT_GRAY);

      //--- Anfang Komponenten
      //JTextArea ausgabe[] = new JTextArea[10];
      jbtnAlleAnzeigen.setText("Alle Artikel anzeigen");
      jbtnAlleAnzeigen.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              jbtnAlleAnzeigen_ActionPerformed(evt);
          }

      });
      panel.add(jbtnAlleAnzeigen);

      jbtnZurueck.setText("zurueck");
      jbtnZurueck.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              jbtnZurueck_ActionPerformed(evt);
          }

      });
      panel.add(jbtnZurueck);
      //--- Ende Komponenten
      
      setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setResizable(false);
      setVisible(true);
    }

    //--- Action-Methoden
    private void jbtnAlleAnzeigen_ActionPerformed(ActionEvent evt) {
      LinkedList<?> artikelliste = null;
      artikelliste = (LinkedList<?>) verw.getAllArticles();
      new ArtikelAnzeigenGUI("Ergebnisfenster", artikelliste);
    }
    private void jbtnZurueck_ActionPerformed(ActionEvent evt) {
      setVisible(false);
      dispose();
    }

    private void configFrame(String title){
      this.setTitle(title);
      int frameWidth  = 300;
      int frameHeight = 300;
      this.setSize(frameWidth, frameHeight);
      Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
      int x = (d.width  - getSize().width)/2;
      int y = (d.height - getSize().height)/2;
      this.setLocation(x+50,y+50);
    }

}
