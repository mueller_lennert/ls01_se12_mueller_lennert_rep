/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//package versandschulegui;
package versandgui;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 *
 * @author goekcen
 */
public class KundenAnzeigenGUI extends JFrame {

  private static final long serialVersionUID = 1L;
  private JButton jbtnSchliessen = new JButton();
  private JTextArea jtarKunde [] = null;
  private JLabel jlblInfo = new JLabel();

  public KundenAnzeigenGUI(String title, LinkedList<?> liste) {
      configFrame(title);
      toFront();

      int anzahlKunden = liste.size();
      //getContentPane().setBackground(Color.);

      //--- Hier Komponenten erzeugen und mit getContentPane().add()
      //--- in das Fenster einf�gen
      Container panel = null;
      panel = getContentPane();

      LayoutManager layout;
      layout = new GridLayout (anzahlKunden+2,1);

      panel.setLayout (layout);
      panel.setBackground (Color.LIGHT_GRAY);

      //--- Anfang Komponenten
      jlblInfo.setText("Das Unternehmen hat "+ anzahlKunden + " registierte Kunden");
      panel.add(jlblInfo);
      
      String ausgabe = "";
      Kundendaten kundenaus = new Kundendaten();
      jtarKunde = new JTextArea[anzahlKunden];
      Font font = new Font(Font.MONOSPACED, Font.PLAIN, 13);
      if (!liste.isEmpty()) {
        for (int i = 0; i < anzahlKunden; i++) {
          jtarKunde[i] = new JTextArea();
          jtarKunde[i].setFont(font);
          kundenaus = (Kundendaten) liste.get(i);
          ausgabe = kundenaus.getKundenNr() + " ";
          ausgabe = ausgabe + kundenaus.getVorname() + " ";
          ausgabe = ausgabe + kundenaus.getNachname();
          jtarKunde[i].setText(ausgabe);
          panel.add(jtarKunde[i]);
        }
      }

      jbtnSchliessen.setText("Schliessen");
      jbtnSchliessen.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              jbtnSchliessen_ActionPerformed(evt);
          }
      });
      panel.add(jbtnSchliessen);

      //--- Ende Komponenten

      setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setResizable(false);
      setVisible(true);
  }
  private void jbtnSchliessen_ActionPerformed(ActionEvent evt) {
    setVisible(false);
    dispose();
  }

  private void configFrame(String title){
    this.setTitle(title);
    int frameWidth  = 300;
    int frameHeight = 400;
    this.setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width  - getSize().width)/2;
    int y = (d.height - getSize().height)/2;
    this.setLocation(x+100,y+100);
  }

}
