/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package versandgui;

//
//
//  Generated by StarUML(tm) Java Add-In
//
//  @ Project : Untitled
//  @ File Name : Kundendaten.java
//  @ Date : 18.11.2010
//  @ Author :
//
//

public class Kundendaten {

    private int kundenNr;
    private String vorname;
    private String nachname;

    public Kundendaten(){
        this.kundenNr = 0;
        this.vorname = "";
        this.nachname = "";
    }

    public Kundendaten(int kundenNr, String vorname, String nachname){
        this.kundenNr = kundenNr;
        this.vorname = vorname;
        this.nachname = nachname;
    }

    public void setKundenNr(int kundenNr){
        this.kundenNr = kundenNr;
    }
    public int getKundenNr(){
        return this.kundenNr;
    }

    public void setVorname(String vorname){
        this.vorname = vorname;
    }
    public String getVorname(){
        return this.vorname;
    }

    public void setNachname(String nachname){
        this.nachname = nachname;
    }
    public String getNachname(){
        return this.nachname;
    }

    @Override
    public String toString(){
        return "" + kundenNr + ")         " + vorname + "      " + nachname;
    }

}
