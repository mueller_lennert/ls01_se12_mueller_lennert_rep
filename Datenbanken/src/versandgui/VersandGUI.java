/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//package versandschulegui;
package versandgui;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
 
@author goekcen
*/
public class VersandGUI extends JFrame {

   VersandVerwaltung verwalt;
   private static final long serialVersionUID = 1L;
   private JButton jbtnKunden  = new JButton();
   private JButton jbtnArtikel = new JButton();
   private JButton jbtnBeenden = new JButton();

   public VersandGUI (String title) {
      //--- Verbindung zur Datenbank
      configDB("Versand");

      //--- Hauptfenster konfigurieren
      setTitle(title);
      configFrame();
      //getContentPane().setBackground(Color.);
      
      //--- Hier Komponenten erzeugen und mit getContentPane().add() in das Fenster einf�gen
      Container panel = null;
      panel = getContentPane ();

      LayoutManager layout;
      layout = new GridLayout (4,1);

      panel.setLayout (layout);
      panel.setBackground (Color.LIGHT_GRAY);
      
      //--- Anfang Komponenten
      //JTextArea ausgabe[] = new JTextArea[10];
      jbtnKunden.setText("Kundenstammdaten");
      jbtnKunden.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              jbtnKunden_ActionPerformed(evt);
          }

      });
      panel.add(jbtnKunden);

      jbtnArtikel.setText("Artikelstammdaten");
      jbtnArtikel.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              jbtnArtikel_ActionPerformed(evt);
          }

      });
      panel.add(jbtnArtikel);

      jbtnBeenden.setText("Beenden");
      jbtnBeenden.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              jbtnBeenden_ActionPerformed(evt);
          }

      });
      panel.add(jbtnBeenden);
      //--- Ende Komponenten
      
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setResizable(false);
      setVisible(true);  
    }

    //--- Action-Methoden
    private void jbtnKunden_ActionPerformed(ActionEvent evt) {
      new KundenGUI("Kundenstammdaten", verwalt);
    }
    private void jbtnArtikel_ActionPerformed(ActionEvent evt) {
      new ArtikelGUI("Artikelstammdaten", verwalt);
    }
    private void jbtnBeenden_ActionPerformed(ActionEvent evt) {
      this.dispose();
    }

    //--- Anfang Methoden
    private void configFrame(){
      int frameWidth  = 200;
      int frameHeight = 200;
      this.setSize(frameWidth, frameHeight);
      Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
      int x = (d.width  - getSize().width)/2;
      int y = (d.height - getSize().height)/2;
      this.setLocation(x-50,y-50);
    }

    private void configDB(String db){
      verwalt = new VersandVerwaltung(db);
      if (verwalt.setupDBConnection()){
         //--- mal schauen
      }
    }
    //--- Ende Methoden



    public static void main(String[] args) {
        // TODO code application logic here
        VersandGUI fenster1 = new VersandGUI("Versand");
        fenster1.setVisible(true);

    }
}
