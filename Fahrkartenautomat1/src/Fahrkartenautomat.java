import java.util.Scanner;

@SuppressWarnings("resource")
class Fahrkartenautomat
{
	
    public static void main(String[] args)
    {
    	
    boolean beenden = false;
    float vorhandenesRestgeld = 60.30f;
    int vorhandeneTickets = 25;
    
    do {
    double[] zuZahlenderBetrag = fahrkartenbestellungErfassen(vorhandeneTickets, vorhandenesRestgeld);
    vorhandeneTickets = (int) zuZahlenderBetrag[0];
    vorhandenesRestgeld = (float) zuZahlenderBetrag[1];
    float eingezahlterBetrag = fahrkartenBezahlen(zuZahlenderBetrag[2]);
    fahrkartenAusgeben();
    vorhandenesRestgeld = (float) rueckgeldAusgeben(zuZahlenderBetrag[2], eingezahlterBetrag, vorhandenesRestgeld)[1]; 
    } while (!beenden);
    
    }
    
    /*Aufgabe A5.3 1.
    Benutzen Sie f�r die Verwaltung der Fahrkartenbezeichnung und der Fahrkartenpreise jeweils ein Array.
    Welche Vorteile hat man durch diesen Schritt?
    Die Verwendung von Arrays ist einfacher als die Verwendung von ganz vielen if-else Abfragen oder einem Switch-Case
    Au�erdem ist es leicher, neue Tickets hinzuzuf�gen oder zu entfernen
    */
    
    /*Aufgabe A5.3 3.
    Vergleich nicht m�glich, weil ich schon von Anfang an Arrays verwendet habe.
    Die Idee, dass mit if-else oder switch zu machen, hatte ich gar nicht, weil das viel zu kompliziert ist.
    */
    
    public static double[] fahrkartenbestellungErfassen(int vorhandeneTickets, float vorhandenesRestgeld) {
        // Bestellung erfassen
        // -----------
    	System.out.println("Fahrkartenbestellvorgang:\r\n=========================");
    	
       Scanner tastatur = new Scanner(System.in);
      
       String[] fahrkartenNamen = {"(1) Einzelfahrt AB", "(2) Einzelfahrt BC", "(3) Einzelfahrt ABC", "(4) Kurzstecke", 
    		   "(5) Tageskarte AB", "(6) Tageskarte BC", "(7) Tageskarte ABC", "(8) Kleingruppe 24-Stunden AB", "(9) Kleingruppe 24-Stunden BC",
    		   "(10) Kleingruppe 24-Stunden ABC", "(11) 4-Fahrten Karte AB", "(11) 4-Fahrten Karte BC",
    		   "(12) 4-Fahrten Karte ABC",  "(14) VBB Flex-Ticket Berlin AB", "(15) BEZAHLEN"};
       float[] preise = {3.0f, 3.5f, 3.8f, 2.0f, 8.8f, 9.2f, 10.0f, 25.5f, 26.0f, 26.5f, 9.4f, 12.6f, 13.8f, 44.0f, 0.0f};
       float zuZahlenderBetrag = 0.0f, aktuellerBetrag = 0.0f;  
       short anzahlTickets = 0, i;
       
       
       do {
       System.out.println("\nListe aller Fahrkarten: ");
       for (i = 0; i < preise.length-1; i++) {
    	   System.out.print(fahrkartenNamen[i] + " " + preise[i] + "�, ");
    	   if (i % 3 == 0 && i != 0 && i != 12 || i == 11) {
    		   System.out.print("\n");
    	   }
       }
       System.out.println(fahrkartenNamen[i] + " " + preise[i] + "�\n");
       
       System.out.print("Geben sie die Kennzahl des gew�nschten Tickets ein: ");
       i = tastatur.nextByte();
       if (i == 16) {
    	   float[] verarbeiten = administratorMen�(vorhandeneTickets, vorhandenesRestgeld);
    	   vorhandeneTickets = (int) verarbeiten[0];
    	   vorhandenesRestgeld = verarbeiten[1];
    	   break;
       }
       i--;

       aktuellerBetrag = preise[i];
       
       if (i+1 < 15 && vorhandeneTickets > 0) {
    	System.out.print("Anzahl der Tickets: ");
       	do {
    	anzahlTickets = tastatur.nextShort();
	   	if (vorhandeneTickets < anzahlTickets && (!(vorhandeneTickets <= 0))) {
		   	System.out.println("Es sind nur noch " + vorhandeneTickets + " Ticket(s) vorhanden.");
		   	System.out.print("Bitte geben Sie eine geringe Ticketanzahl ein: ");
		}
	   	else if (anzahlTickets < 1 || anzahlTickets > 10) {
	   		System.out.print("Bitte geben sie eine Ticketanzahl zwischen 1-10 ein: ");
	   	}
	   	
       	} while ((anzahlTickets < 1 || anzahlTickets > 10) || vorhandeneTickets < anzahlTickets);
       	aktuellerBetrag *= anzahlTickets;
       	zuZahlenderBetrag += aktuellerBetrag;
       	vorhandeneTickets -= anzahlTickets;
       	}
       
	   	if (vorhandeneTickets <= 0 && i+2 <= 15) {
	   		System.out.println("Es sind keine Tickets mehr vorhanden.");
	   		if (anzahlTickets < 1) 
	   			warte(8, true);
	   		System.out.print("\n");
	   		anzahlTickets = 0;
	   	}
       
       } while (i+1 < 15);
       double[] r�ckgabeArray = {vorhandeneTickets, vorhandenesRestgeld, zuZahlenderBetrag};
	   return r�ckgabeArray;
    }

    
    public static float fahrkartenBezahlen(double zuZahlen) {
       // Geldeinwurf
       // -----------
       Scanner bezahlen = new Scanner(System.in);
    
       float eingeworfeneM�nze;
       float eingezahlterGesamtbetrag = 0.0f;
       
       while(eingezahlterGesamtbetrag < zuZahlen)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlen - eingezahlterGesamtbetrag));
    	   eingeworfeneM�nze = bezahlen.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }
       
       
	return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben() {
       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       warte(20, false);
       System.out.println("\n");
    }

	public static void warte(int millisekunde, boolean silent) {
		for (int i = 0; i < 48; i++) {
		if (!silent) {
			System.out.print("=");
		}
	try {
	    Thread.sleep(millisekunde);
	} catch (InterruptedException e) {
		e.printStackTrace();
		} } }

    public static double[] rueckgeldAusgeben(double zuZahlen, double gezahlt, float vorhandenesRestgeld) {
       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       double r�ckgabebetrag = gezahlt - zuZahlen;
       
       byte[] r�ckgabeBetr�ge = new byte[(int) (r�ckgabebetrag * 101)];
       String[] r�ckgabeTypen = new String[(int) (r�ckgabebetrag * 101)];
       int zaehler = 0;
       
       if(r�ckgabebetrag > vorhandenesRestgeld) {
    	   double restbetrag = (vorhandenesRestgeld - r�ckgabebetrag) *(-1);
    	   System.out.printf("Es ist %.2f� zu wenig Restgeld vorhanden!\n", restbetrag); 
       }
       
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro \n", Math.min(vorhandenesRestgeld, r�ckgabebetrag));
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0 && vorhandenesRestgeld >= 2.0) // 2 EURO-M�nzen
           {
        	  r�ckgabeBetr�ge[zaehler] = 2;
        	  r�ckgabeTypen[zaehler] = "EURO";
        	  zaehler++;
	          r�ckgabebetrag -= 2.0;
	          vorhandenesRestgeld -= 2.0f;
	          
           }
           while(r�ckgabebetrag >= 1.0 && vorhandenesRestgeld >= 1.0) // 1 EURO-M�nzen
           {
         	  r�ckgabeBetr�ge[zaehler] = 1;
         	  r�ckgabeTypen[zaehler] = "EURO";
         	  zaehler++;
        	  r�ckgabebetrag -= 1.0;
        	  vorhandenesRestgeld -= 1.0f;
           }
           while(r�ckgabebetrag >= 0.5 && vorhandenesRestgeld >= 0.5) // 50 CENT-M�nzen
           {
          	  r�ckgabeBetr�ge[zaehler] = 50;
          	  r�ckgabeTypen[zaehler] = "CENT";
          	  zaehler++; 
        	  r�ckgabebetrag -= 0.5;
        	  vorhandenesRestgeld -= 0.5f;
           }
           while(r�ckgabebetrag >= 0.2 && vorhandenesRestgeld >= 0.2) // 20 CENT-M�nzen
           {
          	  r�ckgabeBetr�ge[zaehler] = 20;
          	  r�ckgabeTypen[zaehler] = "CENT";
          	  zaehler++;
        	  r�ckgabebetrag -= 0.199;
        	  vorhandenesRestgeld -= 0.199f;
           }
           while(r�ckgabebetrag >= 0.0999 && vorhandenesRestgeld >= 0.0999) // 10 CENT-M�nzen
           {
           	  r�ckgabeBetr�ge[zaehler] = 10;
           	  r�ckgabeTypen[zaehler] = "CENT";
           	  zaehler++;  
        	  r�ckgabebetrag -= 0.0999;
        	  vorhandenesRestgeld -= 0.0999f;
           }
           while(r�ckgabebetrag >= 0.05 && vorhandenesRestgeld >= 0.05)// 5 CENT-M�nzen
           {
           	  r�ckgabeBetr�ge[zaehler] = 5;
           	  r�ckgabeTypen[zaehler] = "CENT";
           	  zaehler++;  
        	  r�ckgabebetrag -= 0.05;
        	  vorhandenesRestgeld -= 0.05f;
           }
       }
       
       muenzeAusgeben(r�ckgabeBetr�ge, r�ckgabeTypen, zaehler);
       
       warte(10, true);
       System.out.print("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       warte(40, true);
       System.out.print("\n\n\n");
       double[] restbetr�ge = {r�ckgabebetrag, vorhandenesRestgeld};
       return restbetr�ge;
	
    	}
    
    	public static void muenzeAusgeben(byte[] betrag, String[] einheit, int menge) {
			for (int i = 0; i < menge; i++) {
				//System.out.println("Es werden insgesamt " + menge + " M�nzen ausgezahlt");
				while(menge >= 3 && i < menge-2) {
				System.out.printf("%s\n%s\n%s %2d %s%s%s%2d %s%s%s%2d %s%s\n %s\n%s\n\n", "   * * *          * * *           * * * ", " *       *      *       *       *       *", 
				"*", betrag[i], einheit[i], " *", "    * ", 
				betrag[i+1], einheit[i+1], " * ", "    * ", 
				betrag[i+2], einheit[i+2], " * ",
				"*       *      *       *       *       *", "   * * *          * * *           * * * ");	
				i += 3;
				}
				while(menge >= 2 && i < menge-1) {
				System.out.printf("%s\n%s\n%s %2d %s%s%s%2d %s%s\n %s\n%s\n\n", "   * * *          * * * ", " *       *      *       * ", 
				"*", betrag[i], einheit[i], " *", "    * ", 
				betrag[i+1], einheit[i+1], " * ",
				"*       *      *       *", "   * * *          * * * ");	
				i += 2;
				}
				while(menge >= 1 && i < menge) {
				System.out.printf("%s\n%s\n%s %2d %s%s%s\n %s\n%s\n\n", "   * * * ", " *       * ", 
				"*", betrag[i], einheit[i], " *", "", 
				"*       * ", "   * * * ");						
				i += 1;
				}
			}
    	}
    	
    	public static float[] administratorMen�(int vorhandeneTickets, float vorhandenesRestgeld) {
    		
    		Scanner tastatur = new Scanner(System.in);
    		
    		System.out.println("\nDas Passwort ist: Passwort123");
    		final String passwort = "Passwort123";
    		String eingabePasswort;
    		
    		do {
        	System.out.print("Geben Sie das Passwort f�r das Administrations-Men� ein: ");
    		eingabePasswort = tastatur.next();
    		} while (!eingabePasswort.equals(passwort));
    		
    		if (passwort.equals(eingabePasswort)) {
    			int auswahl = 0;
    			System.out.printf("Anzahl vorhandener Ticket-Rohlinge: %d\n", vorhandeneTickets);
    			System.out.printf("Menge vorhandenes Restgeld: %.2f�\n\n", vorhandenesRestgeld);
    	       	do {
    	       		System.out.println("W�hlen Sie eins der folgenden Men�punkte aus:");
    	       		System.out.print("(1) Tickets auff�llen, (2) Restgeld nachf�llen, (3) Abbrechen: ");
    	       		auswahl = tastatur.nextInt();
    	       		switch (auswahl) {
    	       		case 1:
    	       		System.out.print("Wie viele Tickets-Rohlinge m�chten Sie nachf�llen? ");
    	       		vorhandeneTickets += tastatur.nextInt();
    	       		System.out.printf("Anzahl vorhandener Ticket-Rohlinge: %d\n\n", vorhandeneTickets);
    	       		break;
    	       		case 2:
        	       	System.out.print("Wie viel Euro Restgeld m�chten Sie nachf�llen? ");
        	       	vorhandenesRestgeld += tastatur.nextFloat();
        	       	System.out.printf("Menge vorhandenes Restgeld: %.2f�\n\n", vorhandenesRestgeld);
    	       		break;
    	       		default:
    	       		break;
    	       		}
    	       	} while (auswahl != 3);
    		}
    		float[] r�ckgabeArray = {vorhandeneTickets, vorhandenesRestgeld};
			return r�ckgabeArray;
    		
    	}
	}
