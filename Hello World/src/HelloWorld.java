public class HelloWorld {

	public static void main(String[] args) {

		System.out.println("Hello World1");
		System.out.println("Hello World2");
		System.out.println("Hello World3");
		
		/*
		 * Zeilenumbrüche in Java sind möglich mit:
		 * println anstatt print
		 * Und \n
		 * 
		 */
		
		System.out.print("Hello World1");
		System.out.print("Hello World2");
		System.out.print("Hello World3");

	}

}
