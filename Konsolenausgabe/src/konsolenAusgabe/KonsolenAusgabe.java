package konsolenAusgabe;

public class KonsolenAusgabe {

	public static void main(String[] args) {
		
		// ----------------------- Aufgabe 1 ----------------------- 
		String wasImWaldSteht = "Der Baum";
		
		System.out.print("Der Baum steht im Wald.");
		System.out.println(" Im Wald stehen viele B�ume");
		
		System.out.print("\n" + wasImWaldSteht + " steht im \"Wald\".\n");
		System.out.println("Im Wald stehen viele B�ume");
		// Die "println" Anweisung f�gt im Gegensatz zu der "print" Anweisung am Ende einen Zeilenumbruch ein
		
		// ----------------------- Aufgabe 2 ----------------------- 
		System.out.print("\n");
		String s = "*";
		System.out.printf("%10s", s);
		System.out.print("\n");
		System.out.printf("%9s", s); System.out.println("**");
		System.out.printf("%8s", s); System.out.println("****");
		System.out.printf("%7s", s); System.out.println("******");
		System.out.printf("%6s", s); System.out.println("********");
		System.out.printf("%5s", s); System.out.println("**********");
		System.out.printf("%4s", s); System.out.println("************");
		System.out.printf("%3s", s); System.out.println("**************");
		System.out.printf("%9s", s); System.out.println("**");
		System.out.printf("%9s", s); System.out.println("**");
		System.out.printf("%9s", s); System.out.println("**");
		
		// ----------------------- Aufgabe 3 -----------------------
		System.out.printf("\n\n%.2f", 22.4234234);
		System.out.printf("\n%.2f", 111.2222);
		System.out.printf("\n%.2f", 4.0);
		System.out.printf("\n%.2f", 1000000.551);
		System.out.printf("\n%.2f", 97.34);
	}

}
