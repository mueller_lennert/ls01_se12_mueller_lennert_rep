package konsolenAusgabe;

public class KonsolenAusgabe {

	public static void main(String[] args) {
		
		// ----------------------- Aufgabe 1 ----------------------- 
		System.out.print("Aufgabe 1:\n");
		System.out.printf("%6s", "**\n");
		System.out.print("*"); System.out.printf("%8s", "*\n");
		System.out.print("*"); System.out.printf("%8s", "*\n");
		System.out.printf("%6s", "**\n");
		
		// ----------------------- Aufgabe 2 ----------------------- 
		System.out.print("\nAufgabe 2:\n");
		String equals = "=";
		String eins = "= 1 ";
		String zwei = "* 2 ";
		String drei = "* 3 ";
		String vier = "* 4 ";
		String f�nf = "* 5";
		
		System.out.printf("%-5s %-19s %-1s %4s\n", "0!", equals, equals, "1");
		System.out.printf("%-5s %-19s %-1s %4s\n", "1!", eins, equals, "1");
		System.out.printf("%-5s %-19s %-1s %4s\n", "2!", eins + zwei, equals, "2");
		System.out.printf("%-5s %-19s %-1s %4s\n", "3!", eins + zwei + drei, equals, "6");
		System.out.printf("%-5s %-19s %-1s %4s\n", "4!", eins + zwei + drei + vier, equals, "24");
		System.out.printf("%-5s %-19s %-1s %4s\n", "5!", eins + zwei + drei + vier + f�nf, equals, "120");
		
		// ----------------------- Aufgabe 3 -----------------------
		System.out.print("\nAufgabe 3:\n");
		System.out.printf("%-12s| %10s", "Fahrenheit", "Celsius\n");
		System.out.println("-----------------------");
		System.out.printf("%+-12d|%10.2f %1s", -20, -28.8889, "\n");
		System.out.printf("%+-12d|%10.2f %1s", -10, -23.3333, "\n");
		System.out.printf("%+-12d|%10.2f %1s", 0, -17.7778, "\n");
		System.out.printf("%+-12d|%10.2f %1s", 20, -6.6667, "\n");
		System.out.printf("%+-12d|%10.2f %1s", 30, -1.1111, "\n");
		
	}

}
