package konsolenAusgabe;

public class Temperaturtabelle {
	
	public static void main(String[] args) {
	
	// ----------------------- Aufgabe 3 -----------------------
	System.out.print("Aufgabe 3:\n\n");
	System.out.printf("%-12s| %10s", "Fahrenheit", "Celsius\n");
	System.out.println("-----------------------");
	System.out.printf("%+-12d|%10.2f %1s", -20, -28.8889, "\n");
	System.out.printf("%+-12d|%10.2f %1s", -10, -23.3333, "\n");
	System.out.printf("%+-12d|%10.2f %1s", 0, -17.7778, "\n");
	System.out.printf("%+-12d|%10.2f %1s", 20, -6.6667, "\n");
	System.out.printf("%+-12d|%10.2f %1s", 30, -1.1111, "\n");
	
	}

}
