package abSchleifen1;

import java.util.Scanner;

public class DivideBy5 {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm pr�ft wie oft eine ganze Zahl durch 5 geteilt werden kann\n");
		
		byte i;
		Scanner sc = new Scanner(System.in);
		System.out.print("Legen Sie die Zahl fest: ");
		long n, zahl = sc.nextLong();
		n = zahl;
		
		for (i = 0; n % 5 == 0; i++) {
			n /= 5;
		}

		System.out.println("Die Zahl " + zahl + " konnte " + i + " Mal durch 5 geteilt werden");
		sc.close();
	}

}
