package abSchleifen1;

public class EinMalEins {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm gibt das kleine EinMalEins aus.\nDies wird mit 2 for-Schleifen umgesetzt.\n");
		int ergebnis;
		
		for (byte zahl1 = 1; zahl1 <= 10; zahl1++) {
			for (byte zahl2 = 1; zahl2 <= 10; zahl2++) {
				ergebnis = zahl1 * zahl2;
				System.out.println(zahl1 + " * " + zahl2 + " = " + ergebnis);
			}
			System.out.print("\n");
		}

	}

}
