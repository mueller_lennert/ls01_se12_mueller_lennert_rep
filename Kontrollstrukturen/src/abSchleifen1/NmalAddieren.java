package abSchleifen1;

import java.util.Scanner;

public class NmalAddieren {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm addiert 1+n bis die eingegebene Zahl erreicht wurde\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Legen Sie die Zahl n fest: ");
		int i, ergebnis = 0, zahl = sc.nextInt();
		
		for (i = 1; i <= zahl; i++) {
			System.out.print(i);
			if (i < zahl) {
				System.out.print(" + ");
			} else if (i == zahl) {
				System.out.print(" = ");
			}
			ergebnis += i;
		}
		
		sc.close();
		System.out.println(ergebnis);
	}
}
