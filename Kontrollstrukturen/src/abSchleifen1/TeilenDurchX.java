package abSchleifen1;

import java.util.Scanner;

public class TeilenDurchX {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm pr�ft, welche Zahlen zwischen 75-150 durch\neine vom Nutzer eingegebene Zahl teilbar sind\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Legen Sie den Teiler fest: ");
		int n = sc.nextInt();
		
		for (int i = 75; i <= 150; i++) {
			if (i % n == 0) {
				System.out.println("Die Zahl " + i + " ist durch " + n + " teilbar");
			}
			
			else if (i % n != 0) {
				System.out.println("Die Zahl " + i + " ist nicht durch " + n + " teilbar");
			}
			
			else {
				System.out.println("Es ist ein Fehler aufgetreten");
			}
		sc.close();
		
		}
	}
}
