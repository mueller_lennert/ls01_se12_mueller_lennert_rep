package abSchleifen1;

public class TeilenDurchZwei {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm pr�ft, welche Zahlen zwischen 1-100 durch zwei teilbar sind\n");
		
		for (int i = 100; i >= 1; i--) {
			if (i % 2 == 0) {
				System.out.println("Die Zahl " + i + " ist durch zwei teilbar");
			}
			
			else {
				System.out.println("Die Zahl " + i + " ist nicht durch zwei teilbar");
			}
			
		}
	}
}
