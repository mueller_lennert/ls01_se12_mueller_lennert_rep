package abSchleifen1;

import java.util.Scanner;

public class Teiler {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm pr�ft, durch welche Zahlen\neine vom Nutzer eingegebene Zahl teilbar ist\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Legen Sie die Zahl fest: ");
		long zahl = sc.nextLong();
		sc.close();
		
		for (long n = zahl; n <= zahl && n > 0; n--) {
			if (zahl % n == 0) {
				System.out.println("Die Zahl " + zahl + " ist durch " + n + " teilbar");
			}
			
			else if (zahl % n != 0) {
				//System.out.println("Die Zahl " + zahl + " ist nicht durch " + n + " teilbar");
			}
			
			else {
				System.out.println("Es ist ein Fehler aufgetreten");
			}
		}
	}
}
