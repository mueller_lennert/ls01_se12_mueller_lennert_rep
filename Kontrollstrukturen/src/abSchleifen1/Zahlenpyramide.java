package abSchleifen1;

import java.util.Scanner;

public class Zahlenpyramide {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm gibt eine Zahlenpyramide mit definierbarer Gr��e aus\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Legen Sie den begrenzenden Wert fest: ");
		int zahl = sc.nextInt();
		
		System.out.print("\n");
		for (int i = 1; i <= zahl; i++) {
			for (int n = 1; n <= i; n++) {
				System.out.print(i);
			}
			System.out.print("\n");
		}
		sc.close();
	}

}
