package auswahlstrukturen;

import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {

	  	System.out.println("Hinweis:");
	  	System.out.println("Das Programm berechnen aus Nutzereingaben den BMI\nund zeigtg die Klassifizierung an");
	  	
	  	double bmi;
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben Sie ihr Gewicht in Kilogramm ein: ");
		float gewicht = sc.nextFloat();
		System.out.print("Geben Sie ihr K�rperh�he in cm ein: ");
		float koerperhoehe = sc.nextFloat();
		System.out.println("Zul�ssige Eingaben sind: \"m\", \"w\" ");
		System.out.print("Geben Sie ihr Geschlecht an?: ");
		char geschlecht = sc.next().charAt(0);
		
		if (geschlecht == 'M' || geschlecht == 'm') {
			bmi = gewicht / Math.pow((koerperhoehe / 100), 2);
			System.out.printf("Ihr BMI ist: %.2f\n", bmi);
			if (bmi < 20) {
				System.out.println("Der BMI ist unter 20. Sie sind untergewichtig.");
			}
			else if (bmi > 20 && bmi <= 25) {
				System.out.println("Der BMI ist zwischen 20 und 25. Ihr Gewicht liegt im Normalbereich.");
			}
			else {
				System.out.println("Der BMI ist �ber 25. Sie sind ein Fettsack.");
			}
		}
		else if (geschlecht == 'W' || geschlecht == 'w') {
			bmi = gewicht / Math.pow((koerperhoehe / 100), 2);
			System.out.printf("Ihr BMI ist: %.2f\n", bmi);
			if (bmi < 19) {
				System.out.println("Der BMI ist unter 19. Sie sind untergewichtig.");
			}
			else if (bmi > 19 && bmi <= 24) {
				System.out.println("Der BMI ist zwischen 19 und 24. Ihr Gewicht liegt im Normalbereich.");
			}
			else {
				System.out.println("Der BMI ist �ber 24. Sie sind fett.");
			}
		}
		else {
			System.out.println("Fehlerhafte Eingabe");
		}
		sc.close();

	}

}
