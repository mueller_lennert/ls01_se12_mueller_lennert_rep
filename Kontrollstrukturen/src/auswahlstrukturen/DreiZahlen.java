package auswahlstrukturen;

import java.util.Scanner;

public class DreiZahlen {

	public static void main(String[] args) {

		//Zweiter Teil von Aufgabe 1 "Eigene Bedingungen"
	  	System.out.println("Hinweis:");
	  	System.out.println("Das Programm gibt die Gr��te von drei eingegeben Zahlen aus.\nJe nachdem, welche Zahl am gr��ten ist wird ggf. noch eine zus�tzliche Meldung ausgegeben");
	  	
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben sie die erste Zahl ein: ");
		int zahl1 = sc.nextInt();
		System.out.print("Geben sie die zweite Zahl ein: ");
		int zahl2 = sc.nextInt();
		System.out.print("Geben sie die dritte Zahl ein: ");
		int zahl3 = sc.nextInt();
		sc.close();
		
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("Die erste Zahl ist gr��er als die beiden anderen Zahlen");
			System.out.println("Die gr��te Zahl ist die erste Zahl: " + zahl1);
		}
		
		else if (zahl3 > zahl2 || zahl3 > zahl1) {
			System.out.println("Die dritte Zahl ist gr��er als Zahl1 oder Zahl2");
			if (zahl2 > zahl1 && zahl2 > zahl3) {
				System.out.println("Die gr��te Zahl ist die zweite Zahl: " + zahl2);
			}
			else {
				System.out.println("Die gr��te Zahl ist die dritte Zahl: " + zahl3);
			}
		}
		
		else {
			if (zahl2 > zahl1 || zahl2 > zahl3) {
				System.out.println("Die gr��te Zahl ist die zweite Zahl: " + zahl2);
			}
			else {
				System.out.println("Alle Zahlen sind gleich gro�");
			}
		}
	}
}
