package auswahlstrukturen;

import java.util.Scanner;

public class Funktionsloeser {

	public static void main(String[] args) {

	  	System.out.println("Hinweis:");
	  	System.out.println("Das Programm wendet je nach eingegeben Wert eine andere Funktion auf die Zahl an");
	  	
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben sie einen Wert f�r \"x\" ein: ");
		double y = 0, x = sc.nextDouble();
		final float E = 2.718f;
		sc.close();

		if (x <= 0) {
			y = Math.pow(E, x);
			System.out.println("\nDer Wert x ist 0 oder kleiner. Die exponentielle Funktion wurde angewendet");
			System.out.println("y = " + y + " = e^x");			
		}
		
		else if (x > 0 && x <= 3) {
			y = Math.pow(x, 2) + 1;
			System.out.println("\nDer Wert x ist gr��er als 0 und kleiner als 3. Die quadratische Funktion wurde angewendet");
			System.out.println("y = " + y + " = x^2 + 1");
		}
		
		else if (x > 3) {
			y = 2 * x + 4;
			System.out.println("\nDer Wert x ist gr��er als 3. Die lineare Funktion wurde angewendet");
			System.out.println("y = " + y + " = 2 * x * 4");
		}
		
		else {
			System.out.println("Fehlerhafte Eingabe..");
		}
		
	}

}
