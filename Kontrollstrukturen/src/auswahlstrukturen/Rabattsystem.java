package auswahlstrukturen;

import java.util.Scanner;

public class Rabattsystem {

	public static void main(String[] args) {
		
	  	System.out.println("Hinweis:");
	  	System.out.println("Das Programm berechnet den Rabatt je nach eingegebenen Bestellwert");
	  	
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben sie den Bestellpreis an: ");
		float bestellwert = sc.nextFloat();
		float neuerBestellwert, rabattInProzent;
		if (bestellwert < 0) {
			System.out.println("Fehler: Der Bestellwert kann nicht unter 0� sein\n");
			main(args);
		}
		
		else {
		if (bestellwert <= 100 && bestellwert > 0) {
			rabattInProzent = 10; 
			neuerBestellwert = bestellwert * (1 -(rabattInProzent/100));
			System.out.printf("Der neue Bestellpreis ist: %.2f�", neuerBestellwert);
		}
		
		else if (bestellwert <= 500 && bestellwert > 100) {
			rabattInProzent = 15; 
			neuerBestellwert = bestellwert * (1 -(rabattInProzent/100));
			System.out.printf("Der neue Bestellpreis ist: %.2f�", neuerBestellwert);
		}
		
		else {
			rabattInProzent = 20; 
			neuerBestellwert = bestellwert * (1 -(rabattInProzent/100));
			System.out.printf("Der neue Bestellpreis ist: %.2f�", neuerBestellwert);
		}
		sc.close();
		
		}
	}
}
