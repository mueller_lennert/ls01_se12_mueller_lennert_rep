package auswahlstrukturen;

import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {

	  	System.out.println("Hinweis:");
	  	System.out.println("Das Programm �berpr�ft ob das eingebene Jahr ein Schaltjahr ist\n");
	  	
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben sie das Jahr an: ");
		int jahr = sc.nextInt();
		sc.close();
		
		if (jahr < 1582) {
			if (jahr < -45) {
				System.out.println("Das eingebene Jahr ist kein Schaltjahr");
			}
			else if (jahr % 4 == 0) {
				System.out.println("Das eingebene Jahr ist ein Schaltjahr");
			}
			else {
				System.out.println("Das eingebene Jahr ist kein Schaltjahr");
			}
		}
		
		else if (jahr >= 1582) { 
			if (jahr % 4 == 0) {
				if (jahr % 100 == 0 && jahr % 400 != 0) {
					System.out.println("Das eingebene Jahr ist kein Schaltjahr");
				}
				else if (jahr % 400 > 0) {
					System.out.println("Das eingebene Jahr ist ein Schaltjahr");
				}
				else {
					System.out.println("Das eingebene Jahr ist ein Schaltjahr");
				}
			} else {
				System.out.println("Das eingebene Jahr ist kein Schaltjahr");
			}
		} else {
			System.out.println("Fehlerhafte Eingabe");
		}

	}

}
