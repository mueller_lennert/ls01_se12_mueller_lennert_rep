package auswahlstrukturen;

import java.util.Arrays;
import java.util.Scanner;

@SuppressWarnings("unused")
public class Sortiere_abc {

	public static void main(String[] args) {

	  	System.out.println("Hinweis:");
	  	System.out.println("Das Programm sortiert Buchstaben und Ziffern aufsteigend\n");
	  	
        Scanner sc = new Scanner(System.in);
        System.out.println("Sobald ein Leerzeichen im Datensatz ist, \nh�rt das Programm auf nach weiteren Zeichen zu suchen");
        System.out.println("Entfernen sie diese (falls vorhanden) vor der Eingabe");
		System.out.print("Geben sie Buchstaben oder Ziffern an: ");
		char[] sortieren = sc.next().replace(" ", "").toCharArray();
		int n = sortieren.length;
		byte zeilenumbruch = 0;
		
		//https://www.geeksforgeeks.org/java-program-to-sort-names-in-an-alphabetical-order/ als Hilfestellung verwendet
        char temp;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                
            	if (Character.compare(sortieren[i], sortieren[j]) > 0) {
                    temp = sortieren[i];
                    sortieren[i] = sortieren[j];
                    sortieren[j] = temp;
                }
            }
        }
		
        //Arrays.sort(sortieren);
        
        System.out.println("\nHier sind die sortierten Eingaben: ");
        for (int i = 0; i < n; i++) {
            System.out.print(sortieren[i] + " ");
            zeilenumbruch++;
            if (zeilenumbruch > 35) {
            	System.out.print("\n");
            	zeilenumbruch = 0;
            }
        }
        sc.close();
        
	}

}
