package auswahlstrukturen;

import java.util.Scanner;

public class Steuersatz {

	public static void main(String[] args) {
		
	  	System.out.println("Hinweis:");
	  	System.out.println("Das Programm addiert je nach Nutzereingabe den erm��igen\noder vollen Mehrwertssteuersatz auf einen Preis");
	  	
	  	float bruttowert;
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben sie den Nettowert an: ");
		float nettowert = sc.nextFloat();
		System.out.println("Zul�ssige Eingaben sind: \"j\", \"n\" ");
		System.out.print("Den erm��igten Mehrwertssteuersatz anwenden?: ");
		char steuersatz = sc.next().charAt(0);
		if (steuersatz == 'J' || steuersatz == 'j') {
			bruttowert = nettowert * 1.07f;
			System.out.printf("Der Bruttopreis ist: %.2f�", bruttowert);
		}
		else if (steuersatz == 'N' || steuersatz == 'n') {
			bruttowert = nettowert * 1.19f;
			System.out.printf("Der Bruttopreis ist: %.2f�", bruttowert);
		}
		else {
			System.out.println("Fehlerhafte Eingabe");
		}
		sc.close();

	}

}
