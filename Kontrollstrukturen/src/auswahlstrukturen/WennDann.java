package auswahlstrukturen;

import java.util.Scanner;

public class WennDann {

	public static void main(String[] args) {
		
		//Erster Teil von Aufgabe 1 "Eigene Bedingungen"
	  	System.out.println("Hinweis:");
	  	System.out.println("Je nachdem wie gro� die beiden eingegebenen Zahlen, wird eine unterschiedliche Meldung ausgegeben.");
	  	
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben sie die erste Zahl ein: ");
		int zahl1 = sc.nextInt();
		System.out.print("Geben sie die zweite Zahl ein: ");
		int zahl2 = sc.nextInt();
		sc.close();
		
		//Wenn-Dann Aktivit�ten aus dem Alltag
		/* Wenn die S-Bahn nicht kommt, dann fahre ich mit dem Bus
		 * Wenn der Drucker nicht funktioniert, dann starte ich ihn neu
		 * Wenn mein Handyakku kaputt ist, mache ich die R�ckseite vom Handy ab und setze einen neuen ein (spa�, geht ja nicht mehr)
		 * Wenn meine Wasserflasche alle ist, f�lle ich sie mit Leitungswasser auf
		 */
		
		if (zahl1 == zahl2) {
			System.out.println("Zahl1 und Zahl2 sind gleich gro�");
		}
		
		if (zahl2 > zahl1) {
			System.out.println("Zahl2 ist gr��er als Zahl1");
		}		
		
		if (zahl1 > zahl2) {
			System.out.println("Zahl1 ist gr��er als Zahl2");
		} else {
			System.out.println("Zahl1 ist nicht gr��er als Zahl2");
		}
	}
}
