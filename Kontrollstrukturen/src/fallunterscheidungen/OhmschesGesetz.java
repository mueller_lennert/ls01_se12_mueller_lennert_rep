package fallunterscheidungen;

import java.util.Scanner;

public class OhmschesGesetz {

	public static void main(String[] args) {

	  	System.out.println("Hinweis:");
	  	System.out.println("Das Programm berechnet je nach Nutzereingabe Spannung, \nWiderstand oder Strommithilfe der URI-Formel");
	  	   
		Scanner sc = new Scanner(System.in);
		System.out.println("Sie k�nnen folgende Gr��en berechnen: U, R und I ");
		System.out.print("Geben sie die zu berechnende Gr��e ein: ");
		char auswaehlen = sc.next().charAt(0);
		float wert1, wert2, ergebnis;
		
		switch (auswaehlen) {
		case 'R': 
		case 'r':
		System.out.print("Geben sie die Spannung ein: ");
		wert1 = sc.nextFloat();
		System.out.print("Geben sie den Strom ein: ");
		wert2 = sc.nextFloat();
		ergebnis = wert1 / wert2;
		System.out.printf("Der Widerstand betr�gt: " + "%.3f Ohm\n", ergebnis); 
		break;
		case 'U': 
		case 'u':
		System.out.print("Geben sie den Widerstand ein: ");
		wert1 = sc.nextFloat();
		System.out.print("Geben sie den Strom ein: ");
		wert2 = sc.nextFloat();
		ergebnis = wert1 * wert2;
		System.out.printf("Die Spannung betr�gt: " + "%.3f Volt\n", ergebnis); 
		break;
		case 'I': 
		case 'i':
		System.out.print("Geben sie die Spannung ein: ");
		wert1 = sc.nextFloat();
		System.out.print("Geben sie den Widerstand ein: ");
		wert2 = sc.nextFloat();
		ergebnis = wert1 / wert2;
		System.out.printf("Der Strom betr�gt: " + "%.3f Ampere\n", ergebnis); 
		break;
		default: System.out.println("Fehlerhafte Eingabe..");
		}
		
		sc.close();

	}

}
