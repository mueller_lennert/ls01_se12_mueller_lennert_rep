package fallunterscheidungen;

import java.util.Scanner;

public class RoemischeZahlen {

	public static void main(String[] args) {

	  	System.out.println("Hinweis:");
	  	System.out.println("Das Programm gibt die entsprechende Dezimalzahl bei Eingabe einer r�mischen Zahl aus");
	  	
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben sie eine r�mische Zahl ein: ");
		char roemischeZahl = sc.next().charAt(0);
		sc.close();
		
		switch (roemischeZahl) {
		case 'I': 
		case 'i':
		System.out.println("I: 1");
		break;
		case 'V': 
		case 'v':
		System.out.println("V: 5");
		break;
		case 'X': 
		case 'x':
		System.out.println("X: 10");
		break;
		case 'L': 
		case 'l':
		System.out.println("L: 50");
		break;
		case 'C':
		case 'c':
		System.out.println("C: 100");
		break;
		case 'D':
		case 'd':
		System.out.println("D: 500");
		break;
		case 'M':
		case 'm':
		System.out.println("M: 1000");
		break;
		default: System.out.println("Fehlerhafte Eingabe..");
		}

	}

}
