package fallunterscheidungen;

import java.util.Scanner;

public class Taschenrechner {
	
	public static void main(String[] args) {
		
		char rechenoperation = ' ';
		Scanner sc = new Scanner(System.in);
		hinweis();
		do {
		double[] zahlen = eingabe(sc, ' ');
		rechenoperation = (char) zahlen[2];
	    double ergebnis = verarbeitung(zahlen[0], zahlen[1], rechenoperation, sc);
		ausgabe(zahlen[0], zahlen[1], ergebnis, rechenoperation);
		} while (rechenoperation != '!');
		
		//Aufgabe 1 (04_AB_Fallauswahl_2016): Ermitteln und erl�utern Sie die Auswirkung einer fehlenden break-Anweisung auf den Programmablauf.
		//Antwort: Bei einer fehlenden break-Anweisung wird weiterer Code im Switch-Statement ausgef�hrt, bis man auf eine break-Anweisung trifft
	}
		
	public static void hinweis() {
	
	System.out.println("Hinweis:");
  	System.out.println("Das Programm wendet die vom Nutzer ausgew�hlte Rechenoperation auf zwei Zahlen an.\n"
  			+ "Der Nutzer kann solange neue Zahlen eingeben bis er das Programm beendet");	
	}
	
	public static double[] eingabe(Scanner sc, char operation) {
		double[] zahlen = {0.0,0.0,0.0};
		
		System.out.print("Erste Zahl eingeben: ");
		zahlen[0] = sc.nextDouble();
		System.out.print("Zweite Zahl eingeben: ");
		zahlen[1] = sc.nextDouble();
		System.out.println("M�gliche Rechenoperationen sind +, -, *, /, % und ?");
		System.out.print("Geben sie die gew�nschte Rechenoperation ein: ");
		
		zahlen[2] = operation = sc.next().charAt(0);
		System.out.print("\n");
		
		return zahlen;
	}
		
		
	public static double verarbeitung(double zahl1, double zahl2, char operation, Scanner sc) {
		double ergebnis = 0;
		switch (operation) {
		case '+':
		ergebnis = addition(zahl1, zahl2);
		break;
		case '-':
		ergebnis = subtraktion(zahl1, zahl2);
		break;
		case '*':
		ergebnis = multiplikation(zahl1, zahl2);
		break;
		case '/':
		case ':':
		ergebnis = division(zahl1, zahl2);
		break;
		case '%':
		ergebnis = modulo(zahl1, zahl2);
		break;
		case '?':
		ergebnis = fragezeichen(zahl1, zahl2);
		break;
		}
		
		System.out.println("Durch Eingabe von \"!\" kann das Programm beendet werden");
		System.out.print("Durch Eingabe von einem beliebgen anderen Zeichen wird das Programm fortgef�hrt ");
		operation = sc.next().charAt(0);
		if (operation == '!') {
			System.exit(0);
		}
		System.out.print("\n");
		return ergebnis;
	}
	
	
	protected static double addition(double zahl1, double zahl2) {
		double ergebnis = zahl1 + zahl2;
		return ergebnis;
	}

	protected static double subtraktion(double zahl1, double zahl2) {
		double ergebnis = zahl1 - zahl2;
		return ergebnis;
	}
	
	protected static double multiplikation(double zahl1, double zahl2) {
		double ergebnis = zahl1 * zahl2;
		return ergebnis;
	}
	
	protected static double division(double zahl1, double zahl2) {
		double ergebnis = zahl1 / zahl2;
		return ergebnis;
	}
	
	protected static double modulo(double zahl1, double zahl2) {
		double ergebnis = zahl1 % zahl2;
		return ergebnis;
	}
	
	protected static double fragezeichen(double zahl1, double zahl2) {
		double ergebnis = Math.abs(Math.sqrt(Math.pow(zahl1, 2) + Math.pow(zahl2, 2)) / Math.sqrt(Math.pow(zahl1, 2) * Math.pow(zahl2, 2)));
		//Je h�her die eingegebenen Zahlen, desto niedriger ist das Ergebnis
		return ergebnis;
	}
	
	public static void ausgabe(double zahl1, double zahl2, double ergebnis, char operation) {
		System.out.printf("%27s\n", "Rechenergebnis"); 
		switch (operation) {
		case '+':
		System.out.println("Ausgabe:                       " + zahl1 + " + " + zahl2 + " = " + ergebnis + "\n");
		break;
		case '-':
		System.out.println("Ausgabe:                       " + zahl1 + " - " + zahl2 + " = " + ergebnis + "\n");
		break;
		case '*':
		System.out.println("Ausgabe:                       " + zahl1 + " * " + zahl2 + " = " + ergebnis + "\n");
		break;
		case '/':
		case ':':
		System.out.println("Ausgabe:                       " + zahl1 + " / " + zahl2 + " = " + ergebnis + "\n");
		break;
		case '%':
		System.out.println("Ausgabe:                       " + zahl1 + " % " + zahl2 + " = " + ergebnis + "\n");
		break;
		case '?':
		System.out.println("Ausgabe:                       " + zahl1 + " ? " + zahl2 + " = " + ergebnis + "\n");
		break;
		}	
	}
}
