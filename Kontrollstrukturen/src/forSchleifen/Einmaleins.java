package forSchleifen;

public class Einmaleins {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm gibt das kleine Einmaleins aus.\nDies wird mit 2 for-Schleifen umgesetzt.\n");
		int ergebnis, zahl1, zahl2 = 1;
		
		System.out.println("      1    2    3    4    5    6    7    8    9   10\n");
		for (zahl1 = 1; zahl1 <= 10; zahl1++) {
			System.out.printf("%-2d", zahl1);
			for (zahl2 = 1; zahl2 <= 10; zahl2++) {
				ergebnis = zahl1 * zahl2;
				System.out.printf("%5d", ergebnis);
			}
			System.out.print("\n");
			
		}

	}

}
