package forSchleifen;

public class Folgen {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm z�hlt bis zur Zahl n oder von n bis 0\n");
		int n = 1;
		
		System.out.println("Schleife A:");
		for (byte i = 99; i >= 9; i -= 3) {
			System.out.print(i + " ");
		}

		System.out.println("\n\nSchleife B:");
		for (int i = 2; n <= 400; i++) {
			System.out.print(n + " ");
			n = i * i;
		}
		
		System.out.println("\n\nSchleife C:");
		for (int i = 2; i <= 102; i += 4) {
			System.out.print(i + " ");
		}
		
		System.out.println("\n\nSchleife D:");
		for (n = 4; n <= 124; n += 8) {
		for (int i = 4; i <= 1024; i += n) {
			System.out.print(i + " ");
			n+=8;
		}}
		
		System.out.println("\n\nSchleife E:");
		for (int i = 2; i <= 32768; i = i * 2) {
			System.out.print(i + " ");
		}	
	}
}
