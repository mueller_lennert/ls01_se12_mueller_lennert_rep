package forSchleifen;

public class Modulo {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm z�hlt von 1 bis 200 und testet dabei,\nob die Zahlen durch 7, 5 oder 4 teilbar sind\n");
		
		for (short i = 1; i < 200; i++) {
			if (i % 7 == 0) {
				System.out.println("Die Zahl " + i + " ist durch 7 teilbar");
			}
			
			if (i % 5 != 0 && i % 4 == 0) {
					System.out.println("Die Zahl " + i + " ist nicht durch 5 aber durch 4 teilbar");
			}
		}
	}	
}
