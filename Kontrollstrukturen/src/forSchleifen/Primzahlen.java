package forSchleifen;

public class Primzahlen {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm pr�ft, durch welche Zahlen\neine vom Nutzer eingegebene Zahl teilbar ist\n");
		int n, x = 0;
		
		System.out.print("P = {");
		for (byte zahl = 2; zahl <= 100; zahl++) {
			for (n = zahl; n <= zahl && n > 0; n--) {
				if (zahl % n == 0) {
					//System.out.println("Die Zahl " + zahl + " ist durch " + n + " teilbar");
					x++;
				}
			
				else if (zahl % n != 0) {
					//System.out.println("Die Zahl " + zahl + " ist nicht durch " + n + " teilbar");
				}
				
				else {
					System.out.println("Es ist ein Fehler aufgetreten");
				}
				
			}
			if (x == 2 && zahl != 97) {
				System.out.print(zahl + ", ");
			}
			else if (x == 2) {
				System.out.print(zahl);
			}
			//System.out.print(x + " " + zahl + " " + n);
			x = 0;
		}
		System.out.print("}");
	}
}
