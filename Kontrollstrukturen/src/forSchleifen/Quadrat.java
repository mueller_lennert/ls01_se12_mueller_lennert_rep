package forSchleifen;

import java.util.Scanner;

public class Quadrat {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm gibt ein Quadrat mit der eingegebenen Lšnge an Zeichen aus\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Legen Sie die Seitenlšnge fest: ");
		int seitenlšnge = sc.nextInt();
		
		//Obere Linie ausgeben
		System.out.print("\n");
		for (int i = 0; i < seitenlšnge; i++) {
			System.out.print("* ");
		}
		System.out.print("\n");
		
		//Mittlere Linie ausgeben
		for (int i = 0; i < (seitenlšnge -2); i++) {
			System.out.printf("*%" + (seitenlšnge *2 -1) + "s", "*\n");
		}		
		
		//Untere Linie ausgeben
		for (int i = 0; i < seitenlšnge; i++) {
			System.out.print("* ");
		}
		System.out.print("\n");
		
		sc.close();
	}

}
