package forSchleifen;

import java.util.Scanner;

public class Sterne {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm gibt die vorgegebene Anzahl an Sternen als Dreieck aus\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Legen Sie die Anzahl der Sterne (*) fest: ");
		int n = -1;
		n += sc.nextInt();
		String sterne = "*";
		
		for (int i = 0; i <= n; i++) {
			System.out.println(sterne);
			sterne += "*";
		}
		sc.close();
	}

}
