package forSchleifen;

import java.util.Scanner;

public class Summe {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm gibt die Summe bestimmter Zahlenfolgen aus\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben Sie bitte einen begrenzenden Wert ein: ");
		int a, b, c, d = 0, e = 0, f = 0, n = sc.nextInt();
		

		for (a = 0; a <= n; a++) {
			d += a;
		} 

		System.out.println("Die Summe f�r A betr�gt: " + d);
		
		for (b = 0; b <= 2*n; b = b+2) {
			e += b;
		}

		System.out.println("Die Summe f�r B betr�gt: " + e);
		
		for (c = 1; c <= (2*n)+1; c = c+2) {
			f += c;
		}

		System.out.println("Die Summe f�r C betr�gt: " + f);
		
		sc.close();
	}
}
