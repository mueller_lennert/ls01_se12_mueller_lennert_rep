package forSchleifen;

public class Test {

	public static void main(String[] args) throws InterruptedException {

		System.out.println("Dies ist ein Testprogramm, um zu schauen wie schnell Java ist");
		System.out.println("In jedem Durchlauf der while-Schleife wird ein Integer um 1 erh�ht\nAller 100 Millionen Zahlen wird der aktuelle Wert ausgegeben");
		System.out.println("Das Programm startet in 8 Sekunden\n");
		Thread.sleep(8000);
		
		int test = 0;
		while (true) {
			test++; 
			if (test % 100000000 == 0) {
				System.out.println(test);
				//Thread.sleep(0, 1);
			}
		}
	}
}
