package forSchleifen;

import java.util.Scanner;

public class Treppe {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm gibt eine Zahlenpyramide mit definierbarer Gr��e aus\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben sie die H�he ein: ");
		int h = sc.nextInt();
		System.out.print("Geben sie die Breite ein: ");
		int b = sc.nextInt();
		
		System.out.print("\n");
		for (int i = h; i >= 1; i--) {
			System.out.printf("%" + (b + 5) + "s", "");
			for (int n = b; n >= i; n--) {
				System.out.printf("*");
			}
			System.out.print("\n");
		}
		sc.close();
	}

	}


