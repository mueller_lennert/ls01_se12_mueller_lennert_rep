package whileSchleifen;

import java.util.Scanner;

public class Fakultaet {

    public static void main(String[] args) {
    	
		System.out.println("Hinweis:");
		System.out.println("Das Programm berechnet eine vom Nutzer eingegebene Fakult�t\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Legen Sie die Fakult�t fest: ");
		int n = sc.nextInt();
        System.out.printf("%d! = %d\n", n, fakultaetBerechnen(n));
        sc.close();
    }
    
    public static long fakultaetBerechnen(long zahl) {
        if (zahl <= 1) {
        	return 1;
        }
        else {
            return zahl * fakultaetBerechnen(zahl - 1);
        }
    }
   
}