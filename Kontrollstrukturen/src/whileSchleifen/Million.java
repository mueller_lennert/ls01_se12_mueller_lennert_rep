package whileSchleifen;

import java.util.Scanner;

public class Million {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm berechnet wie lange es dauert,\nbis man nach Anlegen eines Kapitals Million�r wird.");
		
		Scanner sc = new Scanner(System.in);
		char wiederholen = ' ';
		
		do {
		int jahre = 1;
		double kapitalEnde = 0;
		
		System.out.print("\nWie viel Kapital (in Euro) m�chten Sie anlegen: ");
		float kapitalAnfang = sc.nextFloat();		
		System.out.print("Zinssatz in Prozent: ");
		float zinssatz = sc.nextFloat();
		
		System.out.printf("\nEingezahltes Kapital: %.2f�\n", kapitalAnfang);
		while (kapitalEnde < 1000000) {
			kapitalEnde = kapitalAnfang * Math.pow((1+(zinssatz/100)), jahre);
			System.out.printf("Kapital nach %d Jahre(n): %.2f�\n", jahre, kapitalEnde);
			jahre++;
		}
		System.out.println("Nach " + (jahre-1) + " Jahren sind Sie Million�r!");	
		
		System.out.print("\nM�chten Sie einen weiteren Programmdurchlauf durchf�hren?\nGeben sie 'n' f�r Nein oder ein anderes Zeichen f�r Ja ein: ");
		wiederholen = sc.next().charAt(0);		
		} while (wiederholen != 'n'); 
		sc.close();
	}
	
}
