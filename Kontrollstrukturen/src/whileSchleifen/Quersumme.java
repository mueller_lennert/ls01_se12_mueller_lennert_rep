package whileSchleifen;

import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm berechnet die Quersumme einer Zahl\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben sie bitte eine Zahl ein: ");
		int i = 0, ergebnis = 0, zahl = sc.nextInt();
		sc.close();
		
		while (zahl > 0) {  
			i = zahl % 10;  
			ergebnis = ergebnis + i;  
			zahl = zahl / 10;  
		}  
	
	    System.out.println("Die Quersumme betr�gt: " + ergebnis);
	}
}
