package whileSchleifen;

import java.util.Scanner;

public class Temperaturumrechnung {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm rechnet automatisch Celsius-Werte eines Bereiches\nin Fahrenheit um, und gibt diese als Tabelle aus\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		float start = sc.nextInt();
		System.out.print("Bitte den Endwert in Celsius eingeben: ");
		float ende = sc.nextInt();
		System.out.print("Bitte die Schrittweite in Grad Celsius eingeben: ");
		float fahrenheit = 0.0f, groesse = sc.nextInt();

		while (start <= ende) {
				fahrenheit = (start * 1.8f) + 32;
				System.out.printf("%-6.2f�C %15.2f�F\n", start, fahrenheit);
				start += groesse;
				
			}
		sc.close();
	}
}
