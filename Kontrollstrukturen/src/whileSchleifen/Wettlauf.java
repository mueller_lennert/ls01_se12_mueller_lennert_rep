package whileSchleifen;

public class Wettlauf {

	public static void main(String[] args) {
		
		System.out.println("Hinweis:");
		System.out.println("Das Programm benutzt eine while-Schleife, um\nherauszufinden, wer bei einem Wettberwerb gewinnt\n");

		float SprinterA = 9.5f, SprinterB = 7.0f;
		int meterA = 0, meterB = 250;
		
		while (meterA <= 1000 && meterB <= 1000) {
			meterA += SprinterA;
			meterB += SprinterB;
			System.out.println("SprinterA ist bei " + meterA + "m");
			System.out.println(" SprinterB ist bei " + meterB + "m");
		}
		
		if (meterB > meterA) {
			System.out.println("SprinterB hat als erstes das Ziel erreicht");
		}
		
		else {
			System.out.println("SprinterA hat als erstes das Ziel erreicht");
		}

	}

}
