package whileSchleifen;

import java.util.Scanner;

public class Zaehlen {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm z�hlt bis zur Zahl n oder von n bis 0\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Legen Sie die Zahl n fest: ");
		int n = sc.nextInt();
		System.out.print("M�gliche Eingaben + und -\n");
		System.out.print("Wollen Sie bis n hinauf oder von n hinab z�hlen? ");
		char funktion = sc.next().charAt(0);
		int i = 0;
		
		if (funktion == '+') {
			while (i <= n) {
				System.out.println(i);
				i++;
			}
		}
	
		else if (funktion == '-') {
			i = n;
			while (i >= 0) {
				System.out.println(i);
				i--;
			}
		}
		
		else {
			System.out.println("Fehlerhafte Eingabe..");
		}
		
		sc.close();	
	}
}
