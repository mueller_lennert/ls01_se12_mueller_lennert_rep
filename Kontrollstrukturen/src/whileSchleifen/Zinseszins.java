package whileSchleifen;

import java.util.Scanner;

public class Zinseszins {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Das Programm berechnet wie sich Kapital mit Zinsen �ber die Zeit entwickelt\ndabei beachtet das Programm den Zinseszinseffekt\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Laufzeit (in Jahren) des Sparvertrags: ");
		byte laufzeit = sc.nextByte();
		System.out.print("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		float kapitalAnfang = sc.nextFloat();		
		System.out.print("Zinssatz: ");
		float zinssatz = sc.nextFloat();
		
		System.out.printf("\nEingezahltes Kapital: %.2f�\n", kapitalAnfang);
		
		double kapitalEnde = kapitalAnfang * Math.pow((1+zinssatz/100), laufzeit);
		System.out.printf("Ausgezahltes Kapital: %.2f�\n", kapitalEnde);
		
		sc.close();
	}

}
