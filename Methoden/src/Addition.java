import java.util.Scanner;

public class Addition {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
    	
       double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
 	   programmhinweis();
 	   double[] eingabe = eingabe(zahl1, zahl2);
 	   erg = verarbeitung(eingabe, erg);
 	   ausgabe(eingabe, erg);
    	
    }

    public static void programmhinweis() {
  	   System.out.println("Hinweis:");
  	   System.out.println("Das Programm addiert 2 eingegebene Zahlen.\n");
	}
        
    public static double[] eingabe(double zahl1, double zahl2) {
       System.out.print(" 1. Zahl: ");
       zahl1 = sc.nextDouble();
       System.out.print(" 2. Zahl: ");
       zahl2 = sc.nextDouble();
       double[] eingabe = {zahl1, zahl2};
       return eingabe;
    }
        
    public static double verarbeitung(double[] eingabe, double erg) { 
       //3.Verarbeitung
       erg = eingabe[0] + eingabe[1];
       return erg;
    }
        
    public static void ausgabe(double[] eingabe, double erg) {  
       //2.Ausgabe
       System.out.println("\nErgebnis der Addition");
       System.out.printf("%.2f = %.2f + %.2f", erg, eingabe[0], eingabe[1]);
    }
}
