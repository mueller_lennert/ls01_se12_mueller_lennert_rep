import java.util.Scanner;

public class Kruemelmonster {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		int kekse = 100;
		int anzahl;
		Scanner tastatur = new Scanner(System.in);
		
		Kruemelmonster.sprich();
			
		kekse = Kruemelmonster.friss5(kekse);
		System.out.println("Anzahl Kekse nach dem Fressen von 5 Keksen: " + kekse);
		
		kekse = Kruemelmonster.bring10(kekse);
		System.out.println("Anzahl Kekse nach dem Bringen von 10 Keksen: " + kekse);

		System.out.println("Wie viele Kekse bringt der Nikolaus? ");
		anzahl = tastatur.nextInt();	
		
		kekse = Kruemelmonster.nikolaus(kekse, anzahl);
		System.out.println("Anzahl Kekse nach dem Nikolaus: " + kekse);
		
		kekse = Kruemelmonster.verschenkeDieHaelfte(kekse);
		System.out.println("Anzahl Kekse nach dem Verschenken: " + kekse);
		
		kekse = Kruemelmonster.frissAlleKekse(kekse);
		System.out.println("Anzahl Kekse nach dem Fressen: " + kekse);
		
		kekse = Kruemelmonster.backeAlleKekse(kekse);
		System.out.println("Anzahl Kekse nach dem Backen neuer Kekse: " + kekse);
	}
	
	
	public static void sprich() {
		System.out.println("Ich will Kekseeeee!!!");
	}
	
	public static int friss5(int a) {
		a = a-5;
		return a;
	}
	
	public static int bring10(int b) {
		b = b+10;
		return b;
	}
	
	public static int nikolaus(int kekse, int anzahl) {
		kekse = kekse + anzahl;
		return kekse;
	}
	
	//Das Kr�melmonster hat Hunger und frisst alle Kekse
	//Implementieren Sie die Methode frissAlleKekse()
	protected static int frissAlleKekse(int kekse) {
		kekse -= kekse;
		return kekse;
	}
	
	//Das Kr�melmonster ist heute gro�z�gig
	//es verschenkt die H�lfte seiner Kekse
	//implementieren Sie die Methode verschenkeDieHaelfte
	//und geben Sie den neuen Wert an die Hauptmethode zur�ck
	protected static int verschenkeDieHaelfte(int kekse) {
		kekse = kekse / 2;
		return kekse;
	}
	
	//Das Kr�melmonster ist heute SEHR GRO�Z�GIG
	//und hat unglaublich viele Kekse gebacken
	//Es hat Angst f�r den Rest des Lebens nur noch
	//Kekse backen zu m�ssen, deshalb stellt es
	//die Methode auf privat.
	private static int backeAlleKekse(int kekse) {
		kekse = Short.MAX_VALUE;
		return kekse;
	}
	
}
