import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

	   Scanner sc = new Scanner(System.in);
	   programmhinweis();
	   double[] zahlen = eingabe(sc);
	   double ergebnis = verarbeitung(zahlen);
	   ausgabe(zahlen, ergebnis);
	   
   }
   
   public static void programmhinweis() {
	   System.out.println("Hinweis:");
	   System.out.println("Dieses Programm berechnet den Mittelwert\naus beliebig vielen im Code eingebenen Zahlen\n");
   }
   
   public static double[] eingabe(Scanner sc) {
	   // (E) "Eingabe"
	   // Werte f�r x und y festlegen:
	   // ===========================
	   System.out.print("Von wie vielen Zahlen m�chten sie den Mittelwert berechnen? ");
	   short anzahl = sc.nextShort();
	   double[] zahlen = new double[anzahl+1];
	   System.out.print("Geben sie Ihre Zahlen ein: \n");
	   for (int i = 1; i < anzahl+1; i++) {
		   zahlen[i] = sc.nextDouble();
	   }
	   zahlen[0] = anzahl+1;
	   return zahlen;
   }

   public static double verarbeitung(double[] zahlen) { 
	   // (V) Verarbeitung
	   // Mittelwert von x und y berechnen: 
	   // ================================
	   double gesamtwert = 0;
	   for (int i = 1; i < zahlen[0]; i++) {
		   gesamtwert += zahlen[i];
	   }
	   double ergebnis = gesamtwert / (zahlen[0] -1);
	   return ergebnis;
	   
   }
      
   public static void ausgabe(double[] zahlen, double ergebnis) {   
	   // (A) Ausgabe
	   // Ergebnis auf der Konsole ausgeben:
	   // =================================
	   String zahlenListe = "{";
	   for (int i = 1; i < zahlen[0]; i++) {
		   zahlenListe += zahlen[i]; 
		   if (i < zahlen[0]-1) {
			   zahlenListe += ", ";
		   }
	   }
	   zahlenListe += "}";
   		
	   System.out.printf("\nDer Mittelwert aus den Zahlen %s ist %.2f", zahlenListe, ergebnis);
   }
}
