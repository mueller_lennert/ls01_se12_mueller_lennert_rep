import java.util.Scanner;

public class Multiplikation {

	public static void main(String[] args) {
		
		System.out.println("Hinweis:");
		System.out.println("Dieses Programm multipliziert zwei vom Nutzer angebbare Zahlen miteinander\n");

		Scanner myScanner = new Scanner(System.in);
		System.out.print("Geben Sie die erste Zahl ein: ");
		double a = myScanner.nextDouble();
		System.out.print("Geben Sie die zweite Zahl ein: ");
		double b = myScanner.nextDouble();
		double ergebnis = multiplikation(a, b);
		System.out.println("Ihr Ergebnis: " + a + " * " + b + " = " + ergebnis);
		myScanner.close();

	}
	
	public static double multiplikation(double a, double b) {
		
		double ergebnis = a * b;
		return ergebnis;
		
	}

}
