/* Operatoren.java
   Uebung zu Operatoren in Java
*/
public class Operatoren {
  public static void main(String [] args){
    
	int zahl1, zahl2; 
	/* 1. Vereinbaren Sie zwei Ganzzahlen.*/
    
	zahl1 = 75; zahl2 = 23;
    System.out.println("UEBUNG ZU OPERATOREN IN JAVA\n");
    System.out.println("zahl1: " + zahl1 + " zahl2: " + zahl2 + "\n");
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */
    
    System.out.println("zahl1 + zahl2 = " + (zahl1 + zahl2));
    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */

    System.out.println("zahl1 + zahl2 = " + (zahl1 - zahl2));
    System.out.println("zahl1 + zahl2 = " + (zahl1 * zahl2));
    System.out.println("zahl1 + zahl2 = " + (zahl1 / zahl2));
    System.out.println("zahl1 + zahl2 = " + (zahl1 % zahl2) + "\n");
    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */

    String same = "Ist zahl1 dasselbe wie zahl2? " + (zahl1 == zahl2);
    same = same.replace("true", "ja"); same = same.replace("false", "nein");
    System.out.println(same);
    /* 5. Ueberprüfen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */

    String notsame = "\nIst zahl1 nicht dasselbe wie zahl2? " + (zahl1 != zahl2);
    notsame = notsame.replace("true", "ja"); notsame = notsame.replace("false", "nein");
    System.out.println(notsame);
    String smallerthan = "Ist zahl1 nicht dasselbe wie zahl2? " + (zahl1 < zahl2);
    smallerthan = smallerthan.replace("true", "ja"); smallerthan = smallerthan.replace("false", "nein");
    System.out.println(smallerthan);
    String greaterthan = "Ist zahl1 nicht dasselbe wie zahl2? " + (zahl1 > zahl2) + "\n";
    greaterthan = greaterthan.replace("true", "ja"); greaterthan = greaterthan.replace("false", "nein");
    System.out.println(greaterthan);
    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
    
    if ((zahl1 > 0 && zahl1 < 50) && (zahl2 > 0 && zahl2 < 50)) {
    	System.out.println("Beide Zahlen liegen zwischen 0 und 50");
    }
    else {
    	System.out.println("Mindestens eine der Zahlen liegt nicht zwischen 0 und 50");
    }
    /* 7. Ueberprüfen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */

  }//main
}// Operatoren