import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
				
		programmhinweis();
		String[] eingabe = eingabe();
		String[] berechnung = verarbeitung(eingabe);
		ausgabe(eingabe, berechnung);

	}
	
	public static void programmhinweis() {
	System.out.println("Hinweis:");
	System.out.println("Dieses Programm berechnet den Netto- und Bruttopreis\naus eingegeben Artikeln mit Anzahl\n");
	}
	
	public static String[] eingabe() {
	// Eingabe
	Scanner myScanner = new Scanner(System.in);
	System.out.println("Was m�chten Sie bestellen?");
	String artikel = myScanner.next();
	
	System.out.println("Geben Sie die Anzahl ein:");
	int anzahl = myScanner.nextInt();
	
	System.out.println("Geben Sie den Nettopreis ein:");
	double preis = myScanner.nextDouble();
	
	System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
	double mwst = myScanner.nextDouble();
	
	String anzahl2 = Integer.toString(anzahl);
	String preis2 = Double.toString(preis);
	String mwst2 = Double.toString(mwst);
	
	myScanner.close();
	String[] returnarray = {artikel, anzahl2, preis2, mwst2};
	return returnarray;
	}
		
	public static String[] verarbeitung(String[] eingabe) { 
	// Verarbeiten
	int anzahl = Integer.parseInt(eingabe[1]);
	double preis = Double.parseDouble(eingabe[2]);
	double mwst = Double.parseDouble(eingabe[3]);
	double bruttogesamtpreis = anzahl * preis;
	double nettogesamtpreis = bruttogesamtpreis * (1 + (mwst / 100));
	
	String nettogesamtpreis2 = Double.toString(nettogesamtpreis);
	String bruttogesamtpreis2 = Double.toString(bruttogesamtpreis);
	
	String[] returnarray2 = {nettogesamtpreis2, bruttogesamtpreis2};
	return returnarray2;
	}
	
	public static void ausgabe(String[] eingabe, String[] berechnung) { 
	// Ausgeben
	String artikel = eingabe[0];
	int anzahl = Integer.parseInt(eingabe[1]);
	double mwst = Double.parseDouble(eingabe[3]);
	double nettogesamtpreis = Double.parseDouble(berechnung[0]);
	double bruttogesamtpreis = Double.parseDouble(berechnung[1]);
	
	System.out.println("\t\t\t\t Rechnung");
	System.out.printf("\t Netto:  %-30s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
	System.out.printf("\t Brutto: %-30s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

}
