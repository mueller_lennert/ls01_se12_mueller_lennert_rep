public class Quadrieren {
   
	public static void main(String[] args) {

	   programmhinweis();
	   double eingabe = eingabe();
	   double[] ergebnis = verarbeitung(eingabe);
	   ausgabe(ergebnis);
		
	}
	
	public static void programmhinweis() {
		   System.out.println("Hinweis:");
		   System.out.println("Dieses Programm quadriert eine\nim Code eingebenen Zahl\n");
    }
	
	public static double eingabe() {
		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		double x = 5;
		return x;
	}	
	
	public static double[] verarbeitung(double x) {
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis = x * x;
		double[] array1 = {x, ergebnis};
		return array1;
	}
	
	public static void ausgabe(double[] array1) {
		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x�= %.2f\n", array1[0], array1[1]);
	}	
}
