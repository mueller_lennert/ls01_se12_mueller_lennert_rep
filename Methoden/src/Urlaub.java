import java.util.Scanner;

@SuppressWarnings("resource")
public class Urlaub {

	public static void main(String[] args) {

        programmhinweis();
		Scanner reisegeld = new Scanner(System.in);
		
		System.out.print("Geben sie Ihr Reisegeld ein: ");
		float geld = reisegeld.nextFloat();
		getAction(geld, "");
		
	}
      
	public static void getAction(float geld, String action) {
		Scanner getAction = new Scanner(System.in);
		if (action == null || action == "") {
		System.out.println("\nIn welches Land reisen Sie? Geben sie eines der folgenden L�nder ein: ");
		System.out.print("USA, Japan, England, Schweiz, Schweden | ");
		action = getAction.next(); 
		}
		
		if (action.equalsIgnoreCase("USA") && geld > 100.0f) {
			USA(geld);
		}
		
		else if (action.equalsIgnoreCase("Japan") && geld > 100.0f) {
			Japan(geld);
		}
		
		else if (action.equalsIgnoreCase("England") && geld > 100.0f) {
			England(geld);
		}
		
		else if (action.equalsIgnoreCase("Schweiz") && geld > 100.0f) {
			Schweiz(geld);
		}
		
		else if (action.equalsIgnoreCase("Schweden") && geld > 100.0f) {
			Schweden(geld);
		}
		
		else if (geld <= 100.0f) {
			System.out.println("\nDu all dein Geld ausgegeben und solltest jetzt nach Hause laufen!");
		}
		
		else {
			System.out.println("\nGeben sie eines der oben angezeigen L�nder ein!");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			getAction(geld, "");
		}
		

	}
	
	public static void programmhinweis() {
		System.out.println("Hinweis:");
		System.out.println("Dieses Programm rechnet ihr Ausgaben mit der jeweiligen Reisew�hrung um\n");
	
	}
	
	public static void USA(float eingabe) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("\nWie viel Dollar haben sie dort ausgegeben?: ");
		float ausgaben = tastatur.nextFloat();
		final float USA = 1.22f;
		final String land = "USA";
		float ergebnis = (eingabe - (ausgaben / USA));
		System.out.println("Sie haben noch " + ergebnis + "� Reisegeld �brig.");
		getAction(ergebnis, land);
	}
	
	public static void Japan(float eingabe) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("\nWie viel Yen haben sie dort ausgegeben?: ");
		float ausgaben = tastatur.nextFloat();
		final float Japan = 126.50f;
		final String land = "Japan";
		float ergebnis = (eingabe - (ausgaben / Japan));
		System.out.println("Sie haben noch " + ergebnis + "� Reisegeld �brig.");
		getAction(ergebnis, land);
	}
	
	public static void England(float eingabe) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("\nWie viel Pfund haben sie dort ausgegeben?: ");
		float ausgaben = tastatur.nextFloat();
		final float England = 0.89f;
		final String land = "England";
		float ergebnis = (eingabe - (ausgaben / England));
		System.out.println("Sie haben noch " + ergebnis + "� Reisegeld �brig.");
		getAction(ergebnis, land);
	}
	
	public static void Schweiz(float eingabe) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("\nWie viel Schweizer Franken haben sie dort ausgegeben?: ");
		float ausgaben = tastatur.nextFloat();
		final float Schweiz = 1.08f;
		final String land = "Schweiz";
		float ergebnis = (eingabe - (ausgaben / Schweiz));
		System.out.println("Sie haben noch " + ergebnis + "� Reisegeld �brig.");
		getAction(ergebnis, land);
	}
	
	public static void Schweden(float eingabe) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("\nWie viel Schwedische Kronen haben sie dort ausgegeben?: ");
		float ausgaben = tastatur.nextFloat();
		final float Schweden = 10.10f;
		final String land = "Schweden";
		float ergebnis = (eingabe - (ausgaben / Schweden));
		System.out.println("Sie haben noch " + ergebnis + "� Reisegeld �brig.");
		getAction(ergebnis, land);
	}
	
}
