import java.util.Scanner;

@SuppressWarnings("resource")
public class Volumenberechnung {

	
	public static void main(String[] args) {

		programmhinweis();
		System.out.println("Was m�chten Sie berechnen?");
		System.out.println("w = W�rfel; q = Quader; p = Pyramide; k = Kugel");
		Scanner getAction = new Scanner(System.in);
		String action = getAction.next();		
		
		switch (action) {
		case "w": w�rfelberechnen(); break;
		case "q": quaderberechnen(); break;
		case "p": pyramideberechnen(); break;
		case "k": kugelberechnen(); break;
		default: {
			System.out.println("Geben sie eine der oben angezeigen geometrischen Formen ein!");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
				main(args);
			}
		}
	}
	
		public static void programmhinweis() {
			System.out.println("Hinweis:");
			System.out.println("Dieses Programm berechnet die angegebene geometrische Form mit Nutzerdefinierten Ma�en\n");
			
		}
		
		
		public static void w�rfelberechnen() {
			Scanner tastatur = new Scanner(System.in);
			System.out.println("\nW�rfelberechnung:");
			System.out.print("Geben Sie die L�nge der ersten Seite ein: ");
			double a = tastatur.nextDouble();
			double ausgabe = Math.pow(a, 3);
			System.out.printf("Volumen des W�rfels: %.2f", ausgabe);
			
		}
		
		public static void quaderberechnen() {
			Scanner tastatur = new Scanner(System.in);
			System.out.println("\nQuaderberechnung:");
			System.out.print("Geben Sie die L�nge der ersten Seite ein: ");
			double a = tastatur.nextDouble();
			System.out.print("Geben Sie die L�nge der zweiten Seite ein: ");
			double b = tastatur.nextDouble();
			System.out.print("Geben Sie die L�nge der dritten Seite ein: ");
			double c = tastatur.nextDouble();
			double ausgabe = a * b * c;
			System.out.printf("Volumen des Quaders: %.2f", ausgabe);
			
		}	
		
		public static void pyramideberechnen() {
			Scanner tastatur = new Scanner(System.in);
			System.out.println("\nPyramidenberechnung:");
			System.out.print("Geben Sie die L�nge der Seite ein: ");
			double a = tastatur.nextDouble();
			System.out.print("Geben Sie die H�he ein: ");
			double h = tastatur.nextDouble();
			double ausgabe = Math.pow(a, 2) * h / 3;
			System.out.printf("Volumen der Pyramide: %.2f", ausgabe);
			
		}	
		
		public static void kugelberechnen() {
			Scanner tastatur = new Scanner(System.in);
			System.out.println("\nKugelberechnung:");
			System.out.print("Geben Sie den Radius ein: ");
			double r = tastatur.nextDouble();
			double ausgabe = ((4/3) * Math.pow(r, 3) * Math.PI);
			System.out.printf("Volumen der Kugel: %.2f", ausgabe);
			
		}	
		
	}