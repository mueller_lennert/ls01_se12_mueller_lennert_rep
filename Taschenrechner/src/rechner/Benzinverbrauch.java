package rechner;

import java.util.Scanner;

public class Benzinverbrauch {

	public static void main(String[] args) {

	//Eingabe
		System.out.println("Benzinverbrauch berechnen\n");
		
		//Float wird �berall ben�tigt weil sonst ein ganzzahliges Ergebnis ausgegeben wird.
		Scanner sc = new Scanner(System.in);
		System.out.print("km-Stand beim letzten Tanken: ");
		float zahl1 = sc.nextFloat();
		System.out.print("km-Stand beim Tanken: ");
		float zahl2 = sc.nextFloat();
		System.out.print("Benzinverbrauch (Liter): ");
		float zahl3 = sc.nextFloat();
		sc.close();
		
	//Verarbeitung
		float ergebnis = (zahl3 * 100) / (zahl2 - zahl1);
		
	//Ausgabe
		System.out.printf("\nDas Verbrauch betr�gt %.2f Liter Benzin pro 100 km.", ergebnis);

	}

}
