package rechner;

import java.util.Scanner;

public class Bruttolohn {

	public static void main(String[] args) {

	//Eingabe
		System.out.println("Monatlichen und j�hrlichen Bruttolohn berechnen\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Monatliche Arbeitszeit in Stunden: ");
		short zahl1 = sc.nextShort();
		System.out.print("Stundenlohn in Euro: ");
		float zahl2 = sc.nextFloat();
		sc.close();
		
	//Verarbeitung
		float ergebnis1 = zahl1 * zahl2;
		float ergebnis2 = zahl1 * zahl2 * 12;
	
	//Ausgabe
		System.out.printf("\nDer monatliche Bruttolohn betr�gt %.2f �.\n", ergebnis1);
		System.out.printf("Der j�hrliche Bruttolohn %.2f �.\n", ergebnis2);

	}

}
