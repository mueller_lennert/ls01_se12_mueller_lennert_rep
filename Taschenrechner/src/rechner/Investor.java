package rechner;

import java.util.Scanner;

public class Investor {

	public static void main(String[] args) {

	//Eingabe
		System.out.println("Zinsrechner\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Einzahlung in Euro: ");
		float zahl1 = sc.nextFloat();
		System.out.print("Zinsen in Prozent: ");
		float zahl2 = sc.nextFloat();
		System.out.print("Jahre: ");
		byte zahl3 = sc.nextByte();
		sc.close();
		
	//Verarbeitung
		double ergebnis = zahl1 * Math.abs(1 + Math.abs(zahl2 / 100) * zahl3);
		
		//Diese Berechnungsformel beachtet den Zinseszinseffekt
		//double ergebnis = zahl1 * Math.pow(Math.abs(1 + Math.abs(zahl2 / 100)), zahl3);
	
	//Ausgabe
		System.out.printf("\nDas Endkapital betr�gt %.2f �.", ergebnis);

	}

}
