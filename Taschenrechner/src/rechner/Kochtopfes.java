package rechner;

import java.util.Scanner;

public class Kochtopfes {

	public static void main(String[] args) {

	//Eingabe
		System.out.println("Volumen eines Zylinders berechnen\n");
		
		final float PI = 3.141593f;
		Scanner sc = new Scanner(System.in);
		System.out.print("Durchmesser: ");
		float zahl1 = sc.nextFloat();
		System.out.print("H�he in cm: ");
		float zahl2 = sc.nextFloat();
		sc.close();
		
	//Verarbeitung
		double ergebnis = (PI * Math.pow((zahl1 / 2), 2) * zahl2) / 1000;
	
	//Ausgabe
		System.out.printf("\nDas Volumen betr�gt %.2f Liter.", ergebnis);

	}

}
