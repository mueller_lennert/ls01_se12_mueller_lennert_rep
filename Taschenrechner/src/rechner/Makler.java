package rechner;

import java.util.Scanner;

public class Makler {

	public static void main(String[] args) {
		
	//Eingabe
		System.out.println("Fl�che berechnen\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("L�nge in Meter eingeben: ");
		float zahl1 = sc.nextFloat();
		System.out.print("Breite in Meter eingeben: ");
		float zahl2 = sc.nextFloat();
		sc.close();
		
	//Verarbeitung
		float ergebnis = zahl1 * zahl2;
		
	//Ausgabe
		System.out.printf("Die Fl�che ist %.2f m� gro�.", ergebnis);

	}

}
