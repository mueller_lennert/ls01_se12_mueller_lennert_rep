package rechner;

import java.util.Scanner;

public class Rechner {
	
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Erste Zahl eingeben: ");
		double zahl1 = myScanner.nextInt();
		System.out.print("Zweite Zahl eingeben: ");
		double zahl2 = myScanner.nextInt();
		System.out.print("\n");
		myScanner.close();
		double ergebnis1, ergebnis2, ergebnis3, ergebnis4, ergebnis5;
		
		ergebnis1 = zahl1 + zahl2;
		ergebnis2 = zahl1 - zahl2;
		ergebnis3 = zahl1 * zahl2;
		ergebnis4 = zahl1 / zahl2;
		ergebnis5 = Math.abs(Math.sqrt(Math.pow(zahl1, 2) + Math.pow(zahl2, 2)) / Math.sqrt(Math.pow(zahl1, 2) * Math.pow(zahl2, 2)));
		//Ergebnis5: Je h�her die eingegebenen Zahlen, desto niedriger ist das Ergebnis
		
		System.out.printf("%29s\n", "Rechenergebnisse"); System.out.print("\n");
		System.out.printf("AusgabeADD: " + "%26.2f|", ergebnis1); System.out.print("\n");
		System.out.printf("AusgabeSUB: " + "%26.2f|", ergebnis2); System.out.print("\n");
		System.out.printf("AusgabeMUL: " + "%26.2f|", ergebnis3); System.out.print("\n");
		System.out.printf("AusgabeDIV: " + "%26.2f|", ergebnis4); System.out.print("\n");
		System.out.printf("AusgabeIDK: " + "%26.2f|", ergebnis5); System.out.print("\n\n");
		
	}

}
