package rechner;

import java.util.Scanner;

public class Spielbank {

	public static void main(String[] args) {

	byte chip1, chip2, chip3, chip4, chip5, chip6;
	int ergebnis = 0;
	chip1 = 2; chip2 = 5; chip3 = 10; chip4 = 20; chip5 = 50; chip6 = 100;
	
	//Eingabe
		//Multiplikator um den Schülerrabatt zu berechnen 
		System.out.println("Spielerguthaben berechnen\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Anzahl der Chips: ");
		short zahl1 = sc.nextShort();
		System.out.print("Art der Chips: ");
		byte zahl2 = sc.nextByte();
		sc.close();
		
	//Verarbeitung
		switch(zahl2) {
		
		case 1: ergebnis = zahl1 * chip1; break;
		case 2: ergebnis = zahl1 * chip2; break;
		case 3: ergebnis = zahl1 * chip3; break;
		case 4: ergebnis = zahl1 * chip4; break;
		case 5: ergebnis = zahl1 * chip5; break;
		case 6: ergebnis = zahl1 * chip6; break;
		}
		
	//Ausgabe
		System.out.printf("\nDer Spieler hat %1d € Guthaben.\n", ergebnis);

	}

}
