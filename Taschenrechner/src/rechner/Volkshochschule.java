package rechner;

import java.util.Scanner;

public class Volkshochschule {

	public static void main(String[] args) {

	//Eingabe
		byte stundenpreis = 9;
		//Multiplikator um den Sch�lerrabatt zu berechnen 
		float multiplikator = 0.6f;
		System.out.println("Kursgeb�hren berechnen\n");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Stunden pro Kurssitzung: ");
		byte zahl1 = sc.nextByte();
		System.out.print("Anzahl der Kurssitzungen: ");
		byte zahl2 = sc.nextByte();
		sc.close();
		
	//Verarbeitung
		float ergebnis1 = zahl1 * zahl2 * stundenpreis * multiplikator;
		float ergebnis2 = zahl1 * zahl2 * stundenpreis;
		
	//Ausgabe
		System.out.printf("\nDer gesamte Kurs kostet %.2f � f�r Sch�ler.\n", ergebnis1);
		System.out.printf("Der gesamte Kurs kostet %.2f �.\n f�r Erwachsene", ergebnis2);

	}

}
