package eingabeAusgabe;

/**
  *   Aufgabe: Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  */

public class WeltDerZahlen {

 public static void main(String[] args) {
    
    /* *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 219000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3664088;
    
    // Wie alt bist du? Wie viele Tage sind das?
    int alterTage = 6205; 
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 180000;   
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17075400;
    
    // Wie gro� ist das kleinste Land der Erde?
    float flaecheKleinsteLand = 0.44f;    
    
    /* *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    System.out.println("Anzahl der Sterne: " + anzahlSterne +"km�");
    System.out.println("Einwohner von Berlin: " + bewohnerBerlin);
    System.out.println("Mein Alter in Tagen: " + alterTage);
    System.out.println("Gewicht vom Blauwal: " + gewichtKilogramm);
    System.out.println("Fl�che von Russland: " + flaecheGroessteLand);
    System.out.println("Fl�che von Vatikanstadt: " + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}