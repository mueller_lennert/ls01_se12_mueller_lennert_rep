package arbeitsblatt1;

import java.util.Scanner;

public class CollatzConjecture {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Hinweis:");
		System.out.println("Falls die Zahl gerade ist teilt das Programm durch 2.");
		System.out.println("Falls die Zahl ungerade ist wird mit 3 multipliziert und 1 hinzugefügt.");
		System.out.println("Die Berechnung wird so lange ausgeführt, bis die Zahl 1 ist.");
		System.out.print("Geben Sie den Wert ein: ");
		int n = tastatur.nextInt();
		tastatur.close();
		System.out.print("\n");
		berechnung(n);
	}
		
	public static void berechnung(int n) {
		
		try {
			
		if (n == 1) {
			System.out.println("\nDie Zahl hat den Wert 1 erreicht. " + n);
			return;
		}	
		
		else if (n % 2 == 0) {
			n /= 2;
			System.out.println("Es wurde durch zwei geteilt. Die Zahl ist jetzt: " + n);
			Thread.sleep(1);
			
			berechnung(n);
		}
		
		else if (n % 2 == 1) {
			n = (n * 3) + 1;
			System.out.println("Es wurde mit 3 multipliziert und 1 hinzugefügt. Die Zahl ist jetzt: " + n);
			Thread.sleep(1);
			berechnung(n);
		}	
		
		else {
			System.out.println("\nEs ist ein Fehler aufgetreten. ");
			
		} } catch (InterruptedException e) { 
			e.printStackTrace();
		}
	}
}
