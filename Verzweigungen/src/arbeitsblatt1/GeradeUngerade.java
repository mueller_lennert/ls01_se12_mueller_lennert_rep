package arbeitsblatt1;

import java.util.Scanner;

public class GeradeUngerade {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Hinweis:");
		System.out.println("Dieses Programm pr�ft, ob sie eine gerade oder ungerade Zahl eingegeben haben.");
		System.out.print("Geben Sie den Wert ein: ");
		int a = tastatur.nextInt();
		tastatur.close();
		
		if (a % 2 == 0) {
			System.out.println("\nSie haben eine gerade Zahl eingebgeben. Die Zahl: " + a);
			a /= 2;
			System.out.println("Die Zahl wurde durch zwei geteilt. Die Zahl ist jetzt: " + a);
		}
		
		else if (a % 2 == 1) {
			System.out.println("\nSie haben eine ungerade Zahl eingebgeben. Die Zahl: " + a);
		}	
		
		else {
			System.out.println("\nEs ist ein Fehler aufgetreten. ");
		}	
	}
}
