package arbeitsblatt1;

import java.util.Scanner;

public class GleicheWerte {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		try {
		System.out.println("Hinweis:");
		System.out.println("Dieses Programm vergleicht zwei Eingaben und pr�ft, ob diese denselben Wert haben.\n");
		System.out.print("Geben Sie den ersten Wert ein: ");
		int a = tastatur.nextInt();
		System.out.print("Geben Sie den zweiten Wert ein: ");
		int b = tastatur.nextInt();
		tastatur.close();
		
		if (a == b) {
			System.out.println("\nBeide angegebenen Werte sind gleich");
		}
		
		else if (a != b) {
			System.out.println("\nDie angegebenen Werte sind unterschiedlich");
		}	
		
		else {
			System.out.println("\nEs ist ein Fehler aufgetreten. ");
		}	}
		
		catch (java.util.InputMismatchException e) {
			System.out.println("\nDie beiden Werte haben nicht denselben Datentyp.");
			System.out.println("Dies nennt man dann java.util.InputMismatchException.");
		}

	}
	
}
