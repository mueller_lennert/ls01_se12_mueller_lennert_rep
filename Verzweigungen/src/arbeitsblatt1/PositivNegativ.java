package arbeitsblatt1;

import java.util.Scanner;

public class PositivNegativ {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Hinweis:");
		System.out.println("Dieses Programm pr�ft, ob sie eine positive oder eine negative Zahl eingegeben haben.");
		System.out.print("Geben Sie die Zahl ein: ");
		double a = tastatur.nextDouble();
		tastatur.close();
		
		if (a >= 0) {
			System.out.println("\nSie haben eine positiven Wert eingegeben. Ihr Wert: " + a);
		}
		
		else if (a < 0) {
			System.out.println("\nSie haben eine negativen Wert eingebgeben. Ihr Wert: " + a);
		}	
		
		else {
			System.out.println("\nEs ist ein Fehler aufgetreten. ");
		}	
	}
}
