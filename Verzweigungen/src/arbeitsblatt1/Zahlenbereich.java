package arbeitsblatt1;

import java.util.Scanner;

public class Zahlenbereich {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Hinweis:");
		System.out.println("Dieses Programm pr�ft, ob sie eine kleine, gro�e oder nicht zul�ssige Zahl eingegeben haben.");
		System.out.print("Geben Sie den Wert ein: ");
		double a = tastatur.nextDouble();
		tastatur.close();
		
		if (a > 100.0 || a < 0.0) {
			System.out.println("\nSie haben haben einen Wert au�erhalb des zul�ssigen Zahlenbereichs angegeben.");
			System.out.println("Der zul�ssige Zahlenbereich ist: 0-100, Ihr Wert war: " + a);
		}
		
		else if (a > 50) {
			System.out.println("\nSie haben eine gro�e Zahl eingebgeben. Die Zahl: " + a);
		}	
		
		else if (a < 50) {
			System.out.println("\nSie haben eine kleine Zahl eingebgeben. Die Zahl: " + a);
		}	
		
		else {
			System.out.println("\nEs ist ein Fehler aufgetreten. ");
		}	
	}
}
