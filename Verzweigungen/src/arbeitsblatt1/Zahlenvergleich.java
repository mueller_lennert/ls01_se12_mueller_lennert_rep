package arbeitsblatt1;

import java.util.Scanner;

public class Zahlenvergleich {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Hinweis:");
		System.out.println("Dieses Programm vergleicht zwei Eingaben und pr�ft, ob diese denselben Wert haben");
		System.out.println("oder eine der beiden Zahl gr��er ist. So wird dar�ber entscheiden ob die Zahlen");
		System.out.println("nebeneinander oder untereinander ausgegeben werden.\n");
		System.out.print("Geben Sie den ersten Wert ein: ");
		double a = tastatur.nextDouble();
		System.out.print("Geben Sie den zweiten Wert ein: ");
		double b = tastatur.nextDouble();
		tastatur.close();
		
		if (a == b) {
			System.out.println("Beide Zahlen sind gleich gro�.");
			System.out.println("\n" + a + " " + b);
		}
		
		else if (a < b) {
			System.out.println("Der erste Wert ist gr��er.");
			System.out.println("\n" + a);
			System.out.println("\n" + b);
		}	

		else if (a >= b) {
			System.out.println("Der zweite Wert ist gr��er.");
			System.out.println("\n" + b);
			System.out.println("\n" + a);
		}	
		
		else {
			System.out.println("\nEs ist ein Fehler aufgetreten. ");
		}	
	}
}
