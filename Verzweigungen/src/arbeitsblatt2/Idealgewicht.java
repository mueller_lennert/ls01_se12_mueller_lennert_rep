package arbeitsblatt2;

import java.util.Scanner;

public class Idealgewicht {

	public static void main(String[] args) {

		System.out.println("Hinweis:");
		System.out.println("Dieses Programm berechnet das Idealgewicht, welches\nman bei einer bestimmten K�rpergr��e haben sollte");
		
		Scanner sc = new Scanner(System.in);
		boolean wiederholen = true;
		
		do { 
		System.out.print("Geben sie ihr K�rperh�he an: ");
		
		short koerperhoehe = sc.nextShort();
		if (koerperhoehe < 150 || koerperhoehe > 200) {
			System.out.println("Die K�perh�he muss zwischen 150 und 200cm liegen");
		}
		
		else {
		
		System.out.print("Geben sie ihr Geschlecht an (m/w/x): ");
		char geschlecht = sc.next().charAt(0);
		sc.close();
		int idealgewicht = koerperhoehe - 100;
		
		if (geschlecht == 'm' || geschlecht == 'M') {
			idealgewicht *= 0.9; 
			System.out.println("Ihr Idealgewicht ist: " + idealgewicht +"kg");
		}
		
		else if (geschlecht == 'w' || geschlecht == 'W') {
			idealgewicht *= 0.85; 
			System.out.println("Ihr Idealgewicht ist: " + idealgewicht +"kg");
		}	
		
		else if (geschlecht == 'x' || geschlecht == 'X') {
			System.out.println("Wir k�nnen Ihr Idealgewicht leider nicht berechnen");
		}	
		
		else {
			System.out.println("Fehlerhafte Eingabe");
				}	
			wiederholen = false;
			}
		} while (wiederholen);
	}
}
