package arbeitsblatt2;

import java.util.Scanner;

public class PreisBerechnen {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Hinweis:");
		System.out.println("Dieses Programm berechnet den Preis f�r eine bestimmte Menge von DVD-Rohlingen.\n");
		
		System.out.println("Der Preis f�r einen DVD-R Rohling ist 1.1� bei 1-9 St�ck, ");
		System.out.println("0,95� bei 10-49 St�ck und 0,85� bei mehr als 49 St�ck ");
		System.out.println("Falls der Preis unten 60� liegt, kommen 15� Versandkosten hinzu ");
		System.out.print("Wie viele DVD-R Rohlinge m�chten sie kaufen? ");
		int anzahl = 0;
		anzahl = sc.nextInt();
		sc.close();
		float preis = 0.0f;

		if (anzahl >= 1 && anzahl <= 9) {
			preis = 1.1f * anzahl;
		}

		else if (anzahl >= 10 && anzahl <= 49) {
			preis = 0.95f * anzahl;
		}
		
		else if (anzahl >= 49) {
			preis = 0.85f * anzahl;
		}
		
		else {
			System.out.println("Fehlerhafte Anzahl ");
			return;
		}
		
		if (preis < 60.0) {
			preis += 15.0f;
		}
		
		System.out.println("Gesamtpreis: " + preis +"�");
	}

}
