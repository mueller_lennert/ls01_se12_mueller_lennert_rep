package arbeitsblatt2;

import java.util.Scanner;

public class PunkteInFlensburg {
	
	public static void main(String[] args) {
	
	Scanner sc = new Scanner(System.in);
	
	System.out.println("Hinweis:");
	System.out.println("Dieses Programm berechnet die Anzahl der Punkte, die man bei");
	System.out.println("einer bestimmten Geschwindigkeitsüberschreitung erhalten würde\n");
	
	System.out.println("Aktuell zulässige Höchstgeschwindkeit (innerorts): 50 km/h ");
	System.out.print("Ihre Geschwindigkeit? ");
	final byte MAXIMALE_GESCHWINDIGKEIT = 50;
	short geschwindigkeit = 0;
	geschwindigkeit = sc.nextShort();
	
	if (geschwindigkeit <= MAXIMALE_GESCHWINDIGKEIT+20) {
	System.out.println("Sie erhalten keine Punkte ");
	}
	
	else if (geschwindigkeit <= MAXIMALE_GESCHWINDIGKEIT+25) {
	System.out.println("Sie erhalten 1 Punkt ");
	}
	
	else if (geschwindigkeit <= MAXIMALE_GESCHWINDIGKEIT+40) {
	System.out.println("Sie erhalten 3 Punkte ");
	}
	
	else {
	System.out.println("Sie erhalten 4 Punkte ");
	}
	
	sc.close();
	
	}
}
