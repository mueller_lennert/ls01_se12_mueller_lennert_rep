package arbeitsblatt2;

import java.util.Scanner;

public class Quartal {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("In welchem Monat (1-12) sind Sie geboren? ");
		int monat = 0;
		monat = tastatur.nextInt();
		tastatur.close();
		String ausgabe = "Sie sind im";
		
		if (monat <= 3) {
			ausgabe += " I. Quartal geboren"; 
			System.out.println(ausgabe);
		}
		
		else if (monat <= 6) {
			ausgabe += " II. Quartal geboren"; 
			System.out.println(ausgabe);
		}	
		
		else if (monat <= 9) {
			ausgabe += " III. Quartal geboren"; 
			System.out.println(ausgabe);
		}	
		
		else {
			ausgabe += " IV. Quartal geboren"; 
			System.out.println(ausgabe);
		}	
	}
}
