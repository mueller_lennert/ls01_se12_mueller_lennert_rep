package erstesPackage;

public class ersteKlasse {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		
		//Variablen schreiben sie immer klein
		String behappy = "variable1"; //Deklaration und Initialisierung in einer Variable
		String ausgabe = new String("variable2");
		int zahl; //Deklaration, bedeutet auch: dem System bekannt machen.
		zahl = 5; //Initialisierung
		int zahl2 = 10;
		int zahl3, zahl4;
		zahl3 = 5; zahl4 = 10;
		double kommaZahl;
		kommaZahl = 6.95781234567890;
		
		
		System.out.println("\n");
		String stringZahlentrennung = ", ";
		String stringZahlen = "Die Zahlen sind: ";
		String stringZahl = "5";
		String stringZahl2 = "10";
		//Bevor man was schreibt, muss man immer den Datentyp angeben.
		
		System.out.println("Hello World1\n");
		System.out.println("Hello World2");
		System.out.println("Hello World3");
		
		System.out.println(behappy);
		System.out.print(ausgabe + "\n"); 
		System.out.println(zahl);
		System.out.println(kommaZahl);
		System.out.printf("Unsere Kommazahl: %.2f", kommaZahl);
		//F f�r Formatieren, 2f = f�r zwei Zeichen nach dem Komma
		System.out.println("\n");
		System.out.println("Die Zahl ist: " + zahl); //Konkatenieren = Mehere Ausgaben verbinden
		System.out.println("Die Zahlen sind: " + zahl + ", " + zahl2);
		System.out.println(stringZahlen + stringZahl + stringZahlentrennung + stringZahl2);
		//EVA = Eingabe, Verarbeitung, Ausgabe
		
		System.out.print("Hello World1");
		System.out.print("Hello World2");
		System.out.print("Hello World3\n\n");
		
		for (int i = 45; i > 42; i--) {
			System.out.println("Diese for-Schleife z�hlt r�ckwarts und wird drei mal ausgef�hrt");
		}
		
		for (int i = 0; i < 5; i++) {
			System.out.println("Diese for-Schleife wird f�nf mal ausgef�hrt");
		}

	}

}
